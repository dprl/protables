import cv2
import os
import os.path as osp
import numpy as np
from enum import Enum
from shapely.geometry import Polygon
from shapely.strtree import STRtree
import matplotlib.pyplot as plt
from intervaltree import Interval, IntervalTree
from collections import defaultdict


CC_GRAY_THRESHOLD = 127  # grayscale threshold in [0,255]


PARSE_OUT = "qdgga"
PARSE_TSV_OUT_DIR = osp.abspath(osp.join("%s", PARSE_OUT, "output" ))
VDIR = "view"
HTML_OUT_DIR = osp.abspath(osp.join("%s", VDIR))
SCANSSD_VISUAL_DIR = osp.abspath(osp.join("%s", VDIR, "scanssd"))
TSV_IN_VIS_DIR = osp.abspath(osp.join("%s", VDIR, "qdgga", "input"))
PARSE_DOT_OUT_DIR = osp.join("%s", VDIR, "qdgga", "output")
DETECT_EXP_SYM_IMG_OUT_DIR = osp.abspath(
    osp.join("%s", VDIR, "tsv")
)
SSCR_VIS_DIR = osp.abspath(osp.join("%s", VDIR, "sscraper"))

class DOCUMENT(Enum):
    NAME = 0
    PROTABLE = 1

class REGION(Enum):
    BBOX = 0
    OBJECTS = 1
    TYPE = 2

class OBJECT(Enum):
    MIN_X = 0
    MIN_Y = 1
    MAX_X = 2
    MAX_Y = 3
    LABEL = 4
    TYPE = 5
    SHAPE_INFO = 6


'''
    Take a page of regions, return interval tree to test intersection.
'''
def region_int(region_list):
    t_y = IntervalTree()
    
    # Create interval tree for Y direction 
    # AKS: Debug: Include upper limit (+1 on max value)
    for r in range( len(region_list) ):
        (minX, minY, maxX, maxY) = region_list[r]
        t_y[minY : maxY + 1] = Interval( minX, maxX + 1, r)

    # Return interval tree for all objects on the page.
    return t_y 

################################################################
# Filters
################################################################

##################################
# Regions in PRO tables
##################################
def max_r_area_test(t_width, t_height):
    t_area = t_width * t_height

    # p_check(t_area, "t_area")

    def r_filter(region):
        # Compute region area
        (xMin, yMin, xMax, yMax) = region[REGION.BBOX.value]
        r_width = xMax - xMin + 1
        r_height = yMax - yMin + 1
        r_area = r_width * r_height
        # p_check(r_area, "r_area")

        # Test against minimum area
        if r_area >= t_area:
            return False
        else:
            return True

    # RZ: Debug -- make sure to return the test fn!
    return r_filter

def min_r_area_test(t_width, t_height):
    t_area = t_width * t_height

    # p_check(t_area, "t_area")

    def r_filter(region):
        # Compute region area
        (xMin, yMin, xMax, yMax) = region[REGION.BBOX.value]
        r_width = xMax - xMin + 1
        r_height = yMax - yMin + 1
        r_area = r_width * r_height
        # p_check(r_area, "r_area")

        # Test against minimum area
        if r_area < t_area:
            return False
        else:
            return True

    # RZ: Debug -- make sure to return the test fn!
    return r_filter


################################################################
# CC operations
################################################################
'''
    Extract connected components for PNG page images in a directory,
    belonging to one document ('filename')

    Returns a page/region/object table.
'''
def get_cc_objects_original( input_dir, filename, pageRegionsTable, regionTypeStr ):

    #logging.debug("  * Extracting connected components")

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]
    cc_count = 0
    for p in range( num_pages ):
        #tqdm( range( num_pages ) , desc="  CC Processing"):
        # Read in the page image.
        p_img = cv2.imread(os.path.join(input_dir, str(p+1) + ".png"))
        # p_img = cv2.imread(os.path.join(input_dir, "images", filename, \
        #         str(p+1) + ".png"))
        #pcheck( os.path.join(input_dir, "images", filename, \
        #        str(p+1) + ".png"), "Next file")


        # Construct the p/r/o table region for each region, populate with
        # CCs in each region (within the current page).
        for r in range( len(pageRegionsTable[p] )):
            [ rminX, rminY, rmaxX, rmaxY ] = [ round(c) \
                    for c in pageRegionsTable[p][r]]

            outROTable[p].append( ((rminX, rminY, rmaxX, rmaxY), [], regionTypeStr ) )
            #print( outROTable[p][-1] )

            # Clip region, and then invert B/W.
            clip_img = p_img[ rminY:rmaxY, rminX:rmaxX ]
            inv_img = cv2.bitwise_not( clip_img )

            # Extract list of BBs for connected components
            cc_boxes = extract_ccs_original( inv_img, True )
            #pcheck( cc_boxes, "cc_boxes")

            cc_count += len( cc_boxes )

            # Need to add top-left corner to get page coordinates.
            for (minX, minY, maxX, maxY ) in cc_boxes:
                outROTable[p][r][REGION.OBJECTS.value].append( ( rminX + minX, rminY + minY,
                                                                rminX + maxX, rminY + maxY, '_', 'c' ) )

            #pcheck( outROTable[p][r][1], "table_regions" )

    #logging.debug("    " + filename + ": " + str(cc_count) + " CCs found")
    #show_regionObj_table( outROTable, "CC Table" )

    return outROTable

def get_cc_objects_original_v2( input_dir, pageRegionsTable, regionTypeStr ):

    #logging.debug("  * Extracting connected components")

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]
    page_imgs = [ [] for page in pageRegionsTable ]

    cc_count = 0
    for p in range( num_pages ):
        # RZ: new function to create interval tree for region in page ( Y dir )
        yT = region_int( pageRegionsTable[p] )
        region_area_filter = {}

        # Initialize regions
        for r in range( len(pageRegionsTable[p] ) ):
            [ rminX, rminY, rmaxX, rmaxY ] = \
                [ round(c) for c in pageRegionsTable[p][r] ]
            outROTable[p].append( ( (rminX, rminY, rmaxX, rmaxY), [], regionTypeStr ) )
            r_width = rmaxX - rminX + 1
            r_height = rmaxY - rminY + 1
            region_area_filter[r] = max_r_area_test(r_width, r_height)

        p_img = cv2.imread(os.path.join(input_dir, str(p+1) + ".png"))
        page_imgs[p] = p_img
        inv_img = cv2.bitwise_not( p_img )
        cc_boxes = extract_ccs_original( inv_img, True )

        for bb in cc_boxes:
            (minX, minY, maxX, maxY) = list(map(round, bb))
            yMatches = yT[minY : maxY + 1 ]
            for (begin, end, xInterval) in yMatches:
                (beginX, endX, r) = xInterval
                # No threshold to start; any overlap leads to inclusion
                if xInterval.overlap_size(minX, maxX + 1) > 0 and region_area_filter[r]([bb]):
                   outROTable[p][r][REGION.OBJECTS.value].append( (minX, minY, maxX, maxY, '_', 'c' ) )

        # Construct the p/r/o table region for each region, populate with
        # CCs in each region (within the current page).
#        get_cc_objects_from_img(outROTable=outROTable,
#                                img=p_img,
#                                page=p,
#                                cc_count=cc_count,
#                                pageRegionsTable=pageRegionsTable,
#                                regionTypeStr=regionTypeStr)

    return outROTable, page_imgs


# THis method by ABhisek is no longer used
def get_cc_objects( pdf_img_dir, pageRegionsTable, regionTypeStr, image=False ):

    #logging.debug("  * Extracting connected components")

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]
    outROImgs = [ [] for page in pageRegionsTable ]
    cc_count = 0
    for p in range( num_pages ):
        #tqdm( range( num_pages ) , desc="  CC Processing"):
        # Read in the page image.
        p_img = cv2.imread(os.path.join(pdf_img_dir, \
                str(p+1) + ".png"))
        #pcheck( os.path.join(input_dir, "images", filename, \
        #        str(p+1) + ".png"), "Next file")


        # Construct the p/r/o table region for each region, populate with
        # CCs in each region (within the current page). I.E. SScraper regions
        for r in range( len(pageRegionsTable[p] )):
            # AD Mod: There will be no SScraper CCs for Image Regions
            
            # if len(pageRegionsTable[p][r][1]):
            #     [ rminX, rminY, rmaxX, rmaxY ] = [ round(c) \
            #                 for c in pageRegionsTable[p][r]]
            # else:
            #     rminX, rminY, rmaxX, rmaxY = [ round(c) \
            #                 for c in pageRegionsTable[p][r][0]]

            rminX, rminY, rmaxX, rmaxY = [ round(c) for c in pageRegionsTable[p][r][0]]

            # outROTable[p].append( ([rminX, rminY, rmaxX, rmaxY], [], regionTypeStr ) )
            #print( outROTable[p][-1] )

            # Clip region, and then invert B/W.
            if not image:
                clip_img = p_img[ rminY:rmaxY, rminX:rmaxX ]
                visual = False
                r_bbox = None
            else:
                clip_img = p_img
                visual = True
                r_bbox = [rminX, rminY, rmaxX, rmaxY]

            inv_img = cv2.bitwise_not( clip_img )

            # Extract list of BBs for connected components
            # AD Mod And Add the Actual Region BBox
            if not image:
                cc_boxes = extract_ccs( inv_img, True, visual, r_bbox)
                outROTable[p].append( ([rminX, rminY, rmaxX, rmaxY], [], regionTypeStr ) )
            else:
                cc_boxes, r_bbox = extract_ccs( inv_img, True, visual, r_bbox)
                outROTable[p].append( (r_bbox, [], regionTypeStr ) )
                outROImgs[p].append(p_img[r_bbox[1]:r_bbox[3], r_bbox[0]:r_bbox[2]])

            cc_count += len( cc_boxes )

            # Need to add top-left corner to get page coordinates.
            for (minX, minY, maxX, maxY ) in cc_boxes:
                if not image:
                    outROTable[p][r][1].append( ( rminX + minX, rminY + minY,
                        rminX + maxX, rminY + maxY, '_', 'c' ) )
                else:
                    outROTable[p][r][1].append( ( minX, minY,
                        maxX, maxY, '_', 'c' ) )    


            #pcheck( outROTable[p][r][1], "table_regions" )

    #logging.debug("    " + filename + ": " + str(cc_count) + " CCs found")
    #show_regionObj_table( outROTable, "CC Table" )
    if not image:
        return outROTable
    else:
        return outROTable, outROImgs

def extract_ccs_original(image, norm=False):
    # RZ: Adding parameter to allow standard bounding boxes to be returned
    # in a list.

    # Extract CCs with OpenCV (as 'outer' contours)
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255.0
    ( _, thresh ) = cv2.threshold(image_gray * 255, CC_GRAY_THRESHOLD, 255, 0)
    thresh = np.array(thresh, dtype=np.uint8)

    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    n_contours = len(contours)
    boxes = np.zeros((n_contours, 4), dtype="float")
    labels = ["_"] * n_contours

    nboxes = []
    for ( i, cnt ) in enumerate(contours):
        ( x, y, w, h ) = cv2.boundingRect(cnt)

        if norm:
            nboxes.append( [x, y, x + w, y + h ] )
        else:
            boxes[i, :] = [y, x, y + h, x + w]

    # Modifying output based on [norm]alization flag.
    if norm:
        return nboxes
    else:
        return ( labels, boxes )

# This method by Abhisek is no longer used
# AD Mod: If for Visual ChemScraper, there will be no CC's from SScraper 
# so compute contours from entire image and check intersection
def extract_ccs(image, norm=False, visual=False, r_bbox=None):
    # RZ: Adding parameter to allow standard bounding boxes to be returned
    # in a list.

    # Extract CCs with OpenCV (as 'outer' contours)
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255.0
    ( _, thresh ) = cv2.threshold(image_gray * 255, CC_GRAY_THRESHOLD, 255, 0)
    thresh = np.array(thresh, dtype=np.uint8)

    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    n_contours = len(contours)
    boxes = np.zeros((n_contours, 4), dtype="float")
    labels = ["_"] * n_contours

    nboxes = []
    if visual:
        r_poly = Polygon([(r_bbox[0],r_bbox[1]), (r_bbox[0], r_bbox[3]), (r_bbox[2], r_bbox[3]), (r_bbox[2], r_bbox[1])])
    
    for ( i, cnt ) in enumerate(contours):
        ( x, y, w, h ) = cv2.boundingRect(cnt)

        if norm:
            nboxes.append( [x, y, x + w, y + h ] )
        else:
            boxes[i, :] = [y, x, y + h, x + w]

    # Modifying output based on [norm]alization flag.
    if norm:
        # AD MOD: For Visual ChemScraper: Check for the contours that intersect the region and only add them
        # Then Compute the Actual Formula Region
        if visual:
            nboxes = np.array(nboxes).reshape((-1,4))
            b_polys = [Polygon([(box[0],box[1]), (box[0], box[3]), (box[2], box[3]), (box[2], box[1])]) for box in nboxes]
            tree = STRtree(b_polys, np.arange(0, len(nboxes)))
            indxs = tree.query_items(r_poly)
            nboxes = nboxes[indxs]
            r_minx, r_miny, r_maxx, r_maxy = np.min(nboxes[:,0]), np.min(nboxes[:,1]), np.max(nboxes[:,2]), np.max(nboxes[:,3])
            r_bbox = [r_minx, r_miny, r_maxx, r_maxy]
        return nboxes, r_bbox
    else:
        return ( labels, boxes )
    

# AD Addition for Visual Parser
def refactor_region(contour_boxes):
    contour_boxes = np.array(contour_boxes).reshape((-1,4))
    print(contour_boxes)

    # RZ: Adding parameter to allow standard bounding boxes to be returned
    # in a list.

    # Extract CCs with OpenCV (as 'outer' contours)
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255.0
    ( _, thresh ) = cv2.threshold(image_gray * 255, CC_GRAY_THRESHOLD, 255, 0)
    thresh = np.array(thresh, dtype=np.uint8)

    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    n_contours = len(contours)
    boxes = np.zeros((n_contours, 4), dtype="float")
    labels = ["_"] * n_contours

    nboxes = []
    for ( i, cnt ) in enumerate(contours):
        ( x, y, w, h ) = cv2.boundingRect(cnt)

        if norm:
            nboxes.append( [x, y, x + w, y + h ] )
        else:
            boxes[i, :] = [y, x, y + h, x + w]

    # Modifying output based on [norm]alization flag.
    if norm:
        return nboxes
    else:
        return ( labels, boxes )
