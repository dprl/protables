# PROTables: Page-Region-Object Tables  

The PROTables utility library is designed to work with the TSV-based PRO file format 
designed and used by the
[Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html) to specify regions in a document 
and their associated information. It contains IO routines, as well as functionality for sorting and filtering document regions (e.g., formulas) and their associated objects/contents (e.g., mathematical symbols).

The file format is a simple tab-separated variable (TSV) format:

```
D   [doc_no]    [file_path]
P   [page_no]   
FR  [reg_no]    minX        minY            maxX    maxY
[obj_type]      [obj_no]    [label/class]   minX    minY    maxX    maxY
...
```
Multiple documents, pages, and regions can appear in the file. All documents, pages, regions, and objects are numbered, with objects numbered starting from '1' in each region.  Regions and objects in regions have associated bounding boxes ( [ minX, minY, maxX, maxY ]).

**Objects** Objects inside regions can be of arbitrary type, with type indicated by a symbol in the leftmost column. Objects also have an associated label (or class). So for example, a symbol might be the letter 't', and so we use that as its label. For connected components in an image that not have been classified, we can use underscore ('_') to represent that no label has been assigned.

So far in our own work, we have used 'c' for connected components, 'S' for symbols, 'R' for relationships (e.g., for superscripts, overloading bounding box coordinates to represent object ids). We use other types to record the visual syntax (structure) for formulas in MathML or Label Graph (lg) formats, using 'MML' and 'lg' type labels, with the MathML/LG string recorded as the label for the object, and using negative coordinates as placeholders for the bounding box values (e.g., [ -1, -1, -1, -1 ], with different negative values being used to produce different orders when the file is viewed). 



## Local install
```zsh
$ conda create -n protables python=3.6.9
$ conda activate protables
(protables) $ pip install -r requirements.txt
(protables) $ git clone https://gitlab.com/dprl/lgeval.git
```
## Install as a dependency

## Testing

## Contributors

Protables was developed by the [Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html) at the Rochester Institute of Technology (USA), for the [MathSeer](https://www.cs.rit.edu/~dprl/mathseer) project by the following people:


 * Richard Zanibbi 
 * Matt Langenkamp
 * Ayush Kumar Shah

## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.
