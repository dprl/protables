import os
import os.path as osp
import sys
from glob import glob
# it is assumed that lgeval is added to pythonpath
from lgeval.src import lg2txt
import cv2
import datetime

# it is assumed that lgeval is added to pythonpath
try:
    LG_DIR = [p for p in os.environ["PYTHONPATH"].split(":") if "lgeval" in p][0]
except:
    LG_DIR = "lgeval"
LABELS_TRANS_CSV = osp.join(LG_DIR, "translate", "infty_to_crohme.csv")
MATHML_MAP_DIR = osp.join(LG_DIR, "translate", "mathMLMap.csv")


def get_mml(filename, translate=True):
    # print(osp.basename(filename))
    try:
        mml_out = lg2txt.main(filename, MATHML_MAP_DIR, LABELS_TRANS_CSV)
    except Exception as e:
        print("  >> MML convert error for: " + osp.basename(filename))
        mml_out = ""
    # print()
    return mml_out


def create_mainHTML(html_dir, input_dir, filenames, file_num_pages):
    # Style
    css_dir = os.path.join(html_dir, "css")
    if not os.path.exists(css_dir):
        os.makedirs(css_dir)

    with open(os.path.join(css_dir, "style.css"), 'w') as cssStream:

        cssStream.write('<style type="text/css">\n')
        cssStream.write('svg { overflow: visible; }\n')
        cssStream.write('p { line-height: 125%; }\n')
        cssStream.write('li { line-height: 125%; }\n')
        cssStream.write('button { font-size: 12pt; }\n')
        cssStream.write(
            'td { font-size: 10pt; align: left; text-align: left; border: 1px solid lightgray; padding: 5px; }\n')
        cssStream.write(
            'th { font-size: 10pt; font-weight: normal; border: 1px solid lightgray; padding: 10px; background-color: '
            'lavender; text-align: left }\n')
        cssStream.write('tr { padding: 4px; }\n')
        cssStream.write('table { border-collapse:collapse;}\n')
        cssStream.write('</style>')

    with open(osp.join(html_dir, "index.html"), 'w') as htmlStream:

        htmlStream.write('<!DOCTYPE HTML>\n')
        htmlStream.write('<meta charset="UTF-8">\n<html xmlns="http://www.w3.org/1999/xhtml">\n')
        htmlStream.write('<head>\n')
        htmlStream.write('</head>\n\n')
        htmlStream.write("<font face=\"helvetica,arial,sans-serif\">\n")
        htmlStream.write("<h2> Graphics Extraction Results Visualization </h2>\n")
        htmlStream.write(str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "<br>\n")
        htmlStream.write("<b>Input Dir:</b> " + input_dir + "<HR>\n")
        htmlStream.write("<h3> PDF Files Processed (" + str(len(filenames))
                         + "): </h3> \n")
        htmlStream.write("<ol>\n")

        # Create collapsible lists for files
        for (i, filename) in enumerate(filenames):
            htmlStream.write("<li>\n<details>\n<summary>" + filename + "</summary>\n")
            if file_num_pages:
                htmlStream.write("<ol type=\"i\">\n")
                # print(filename, file_num_pages[i] + 1)
                for p_num in range(1, file_num_pages[i] + 1):
                    #regionCount = len( proTable[ p_num - 1 ] )
                    r_count_string = ''
                    # if regionCount > 0:
                    #    rcount_string = ' :: ' + str(regionCount) + ' region(s)'
                    htmlStream.write("<li><A href='qdgga-parser/output/" + filename + "/00_Page_" +
                                     str(p_num) + ".html'>Page " + str(p_num) +
                                     "</A>" + r_count_string + "</li>\n")
            else:
                htmlStream.write("No formula regions found.")
            htmlStream.write('</ol>\n</details>\n')
        htmlStream.write('</ol>\n')
        htmlStream.write('</html>')


# RZ: Separating for clarity
def write_header(html_stream, input_dir, filename, p_num, region_count, pdf_img_dir, num_pages):
    sp_num = str(p_num)

    # Write HTML header, title material
    html_stream.write('<!DOCTYPE HTML>\n')
    html_stream.write('<meta charset="UTF-8">\n<html xmlns="http://www.w3.org/1999/xhtml">\n')
    html_stream.write('<head>\n')
    html_stream.write("<link rel=\"stylesheet\" href=\"css/style.css\">\n")

    # For rendering mathml formulas
    html_stream.write("<script src=\"https://polyfill.io/v3/polyfill.min.js?features=es6\"></script>\n")
    html_stream.write("<script type=\"text/javascript\" id=\"MathJax-script\" ")
    html_stream.write("async src=\"https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js\"></script>\n")
    html_stream.write('</head>\n\n')
    html_stream.write("<font face=\"helvetica,arial,sans-serif\">\n")
    html_stream.write("<h2> Graphics Extraction Results Visualization </h2>\n")
    html_stream.write(str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "<br>\n")
    html_stream.write("<b>Input Dir:</b> " + input_dir + "\n<hr>\n")

    html_stream.write("<h3> ( " + filename + ", Page " + sp_num +
                      " )   Formulas: " + str(region_count) + "</h3>\n")

    # Navigation buttons
    prev_disabled = ''
    next_disabled = ''
    if p_num < 2:
        prev_disabled = "disabled"
    if p_num >= num_pages:
        next_disabled = "disabled"

    html_stream.write("<button onclick=\"location.href='../../../index.html'\" type=\"button\"> \n" "Home</button> \n")
    html_stream.write("<button onclick=\"location.href='00_Page_" + str(p_num - 1) + ".html'\" type=\"button\"" +
                      prev_disabled + "> \n" "Prev ""Page</button> \n")

    html_stream.write("<button onclick=\"location.href='" + "00_Page_" + str(
        p_num + 1) + ".html'\" type=\"button\"" + next_disabled + "> \n"
                                                                  "Next Page</button> &nbsp;&nbsp;&nbsp;&nbsp; "
                                                                  "<b>Page-Level Views:</b> <a "
                                                                  "href='../../../scanssd/" + filename + "/"
                      + str(p_num) + ".png'\">Detector</a> -- <a href='../../../sscraper/" + filename + "/"
                      + str(p_num) + ".png'\">Detector + PDF Symbols </a> -- <a href=\"../../../qdgga-parser/input/"
                      + filename + "/" + str(p_num)
                      + ".png\">Parser Input</a> <br><p><b>Color Scheme:</b> Box colors indicate formulas (blue), "
                      "PDF symbols (green), connected components (red), and PDF words are cyan in the 'Merged' "
                      "view.</p><br><br>\n")

    # Page image 
    html_stream.write('<table valign="top" style="width:100%;">\n' +
                      '<tr><th>Parser Formula Region Input</th> <th>PDF Symbols / CCs + Formula Structure '
                      'Results</th>\n' +
                      '   <tr valign="top">\n' +
                      '       <td>\n' +
                      '               <img border=2 width=500 src="../../../qdgga-parser/input/' + filename + '/' + str(p_num)  +
                      '.png' + '">\n' +
                      '       </td>\n' +
                      '       <td style="width:100%;">\n')

    # Table of recognition data
    html_stream.write('       <div style="height:100vh; width:62vw; overflow-y:scroll;">')
    html_stream.write('         <table border=2 style="width:100%;" valign="top">\n')
    html_stream.write('           <tr>\n<th>Input / Output </th>\n')
    html_stream.write('             <th>Symbol Layout Tree </th></tr>\n')


# RZ: moved here for clarity.
# Page and formula numbers from 1 (not 0)
def write_formula_entry(html_stream, filename, pro_table, output_dir,
                        tsv_out_dir, p_num, f_num, dot_pdf_dir, input_dir, width_scale,
                        height_scale):
    # Generate file names
    sp_num = str(p_num)
    exp_name = (filename + "-P" + sp_num + "-R" + str(f_num))
    exp_img = exp_name + '.PNG'
    dot_file = exp_name + '.pdf'

    # Get (formula) region information
    (minX, minY, maxX, maxY) = pro_table[p_num - 1][f_num - 1][0]
    orig_width = maxX - minX
    orig_height = maxY - minY

    padding = 10  # pixels
    width_scaled = str(round(orig_width * width_scale) + 2 * padding)
    min_x_scaled = str(round(minX * width_scale) - padding)

    height_scaled = str(round(orig_height * height_scale) + 2 * padding)
    min_y_scaled = str(round(minY * height_scale) - padding)

    # MML
    region_entry_list = pro_table[p_num - 1][f_num - 1][1]

    mathml = '$Missing$'
    if len(region_entry_list) > 0 and region_entry_list[-1][5] == 'MML':
        # Use [1:-1] to remove start/end quotes
        mathml = region_entry_list[-1][4][1:-1].replace('\\\\', '\\').replace('\\n', '') \
            .replace('\t', '')

    slt_string = 'SLT Missing'
    dot_path = os.path.join(dot_pdf_dir, filename, dot_file)
    dot_pdf_string = dot_file #'qdgga-parser/output/' + filename + '/' + dot_file
    if os.path.exists(dot_path):
        slt_string = '<iframe style="height:200px; width:600px;" src="' + dot_pdf_string + '"></iframe>'

    # Write table row entry
    html_stream.write('<tr>\n')
    html_stream.write('<td><table><tr><td><b>' + exp_name + \
                      '</b></td></tr>\n')

    # Rendered input image path (RELATIVE TO HTML DIRECTORY)
    input_tsv_img = os.path.join('../../input', filename, sp_num + ".png")
    input_ssd_img = os.path.join('../../../scanssd', filename, sp_num + ".png")
    #input_qdgga_img = os.path.join('qdgga-parser/input', filename, sp_num + ".png")

    # print( ">> " + input_tsv_img )
    # input("  Press enter...")

    # Margins give y offset, then x offset; Show SSD and TSV input images
    html_stream.write('<tr><td>Detector<br>\n')
    html_stream.write('  <div style="width:' + width_scaled +
                      'px; height:' + height_scaled + 'px; overflow:hidden; border: 2px solid;">\n')
    html_stream.write(
        '    <img src="' + input_ssd_img + '" style="margin-top:-'
        + min_y_scaled + 'px; margin-left:-' + min_x_scaled + 'px;" />\n')
    html_stream.write('</div>\n')
    html_stream.write('<br>Parser Input (TSV)\n')

    html_stream.write('  <div style="width:' + width_scaled +
                      'px; height:' + height_scaled + 'px; overflow:hidden; border: 2px solid;">\n')
    html_stream.write(
        '    <img src="' + input_tsv_img + '" style="margin-top:-'
        + min_y_scaled + 'px; margin-left:-' + min_x_scaled + 'px;" />\n')
    html_stream.write('</div>\n<br></td></tr>\n')

    #         '<tr><td><img height=100 src="' + dot_file +
    #                 '" style="width:auto;"></td></tr>\n')
    html_stream.write('<tr><td>Parser Output (MathML)<br><br>\n<strong style="font-size: 18px;">\n')
    html_stream.write('<div style="border:2px solid;padding:2px;"><math xmlns="http://www.w3.org/1998/Math/MathML">\n')
    html_stream.write(mathml + '</math></div>\n</strong>\n</td></tr></table><br><br></td>')
    html_stream.write('<td>' + slt_string + '</td></tr>\n')


# RZ: Modify to read MathML from TSV file.
# Write separate HTML pages for each page of a PDF.
def create_pdfHTML(pro_table, input_dir, output_dir, pdf_html_dir,
                   filename, num_pages, pdf_img_dir, exp_sym_img_dir,
                   exp_img_dir, dot_pdf_dir, tsv_out_dir, MSP, img: list=None):
    if not osp.exists(pdf_html_dir):
        os.makedirs(pdf_html_dir)

    if img is None:
    # Construct directories
        pdf_img_dir = osp.join('../../..', pdf_img_dir)

        # Determine image scales (ASSUMPTION: THESE ARE FIXED FOR THE DOC)
        orig_img = cv2.imread(os.path.join(input_dir, "images", filename,
                                           "1.png"))
    else:
        orig_img = img
    (origHeight, origWidth, _) = orig_img.shape

    annot_img = cv2.imread(os.path.join(MSP.TSV_IN_VIS_DIR % output_dir, filename, "1.png"))
    (annotHeight, annotWidth, _) = annot_img.shape

    # Iterate over pages.
    if not pro_table:
        return
    for p_num in range(1, num_pages + 1):
        sp_num = str(p_num)
        region_count = len(pro_table[p_num - 1])

        # Compute scaled box sides and top-left corner.
        # Computing scales separately, to avoid boundary/rounding differences.
        # (ASSUMED to be pertinent...)
        width_scale = annotWidth / origWidth
        height_scale = annotHeight / origHeight

        with open(osp.join(pdf_html_dir, "00_Page_" + sp_num + ".html"), 'w', encoding='utf8') as \
                htmlStream:

            # RZ: header dat writing in separate function
            write_header(htmlStream, input_dir, filename, p_num, region_count, pdf_img_dir, num_pages)

            # Count number of formula regions for page
            num_exp = len(pro_table[p_num - 1])

            # RZ: modified to use PRO data
            for f_num in range(1, int(num_exp) + 1):
                write_formula_entry(htmlStream, filename, pro_table,
                                    output_dir, tsv_out_dir, p_num, f_num, dot_pdf_dir,
                                    input_dir, width_scale, height_scale)

            htmlStream.write('</table>\n</div>\n</td>\n</tr>')
            htmlStream.write('</table>\n')
            htmlStream.write('</html>')


if __name__ == "__main__":
    # html_dir = sys.argv[1]
    # filename = sys.argv[2]
    pass
