import pytest
from ProTables import *
import os
from io import StringIO
import shutil
from util import *
import util as MSP


def test_from_file():
    """
    loads in a tsv file and confirms certain fields are correct
    :return:
    """
    test_file = os.path.join('examples', 'A00-3007.tsv')
    ( pro_table, docs_page_sizes ) = read_pro_tsv(test_file)
    # document is correct
    assert 'A00-3007' in pro_table[0][0]
    # number of pages is correct
    assert len(pro_table[0][1]) == 6
    # number of formulas on the second page is correct
    assert len(pro_table[0][1][1]) == 9


def test_from_bytes():
    """
    loads a tsv from bytes as if it was a https response and confirms certain fields are correct
    :return:
    """
    test_file = os.path.join('examples', 'A00-3007.tsv')
    fp = open(test_file)
    pro_io = StringIO(fp.read())
    ( pro_table, docs_page_sizes ) = read_pro_tsv_from_io(pro_io)
    # document is correct
    assert 'A00-3007' in pro_table[0][0]
    # number of pages is correct
    assert len(pro_table[0][1]) == 6
    # number of formulas on the second page is correct
    assert len(pro_table[0][1][1]) == 9


def test_save_from_bytes():

    symbol_map = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}
    test_file = os.path.join('examples', 'A00-3007.tsv')
    fp = open(test_file)
    pro_io = StringIO(fp.read())
    ( pro_table, docs_page_sizes ) = read_pro_tsv_from_io(pro_io)
    write_it_pro_tsv("test_write_pro", "examples", "tmp_out",
                     (symbol_map, pro_table[0][1]))


def test_create_html():

    create_htmls(["A00-3007"], "examples", osp.join("examples", "outputs", "ACL"), model_name="no", last_epoch="89", MSP=MSP)


def test_create_from_csv_and_xml():
    img = cv2.imread(os.path.join("examples", "images", "A00-3007", "1.png"))
    (png_height, png_width, _) = img.shape
    symbolMap = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}
    (num_pages, pageCharBBs, pageWordBBs) = \
        read_sscraper_xml("examples", "A00-3007", png_width, png_height)
    # pcheck( filename )
    # pcheck( str(pageWordBBs ) )

    # Bounding boxe from ScanSSD -- filter boxes that are too thin.
    print(num_pages)
    pageMathBBs = read_region_csv(osp.join("examples", "A00-3007.csv"), num_pages)
    bb_thick_test = min_bb_thickness(2, 2)
    fPageMathBBs = filter_bb_list(bb_thick_test, pageMathBBs)

    # Find ScanSSD and SymbolScraper word region intersections
    # Grow regions to hold words.
    regionWordPROTable = \
        region_intersection(fPageMathBBs, 'FR', pageWordBBs)
    grownRegionWordPROTable = transform_pr_table(
        grow_for_contents(png_width, png_height),
        None, regionWordPROTable)
    regionWordBBs = region_bb_list(grownRegionWordPROTable)

    # Merge characters into the regions grown around words chars are from
    regionCharPROTable = \
        region_intersection(regionWordBBs, 'FR', pageCharBBs)
    regionCharWordPROTable = \
        merge_pro_tables(grownRegionWordPROTable, regionCharPROTable)

    # Save TSV for words/chars from PDF in formula regions at this point.
    write_it_pro_tsv("temp_out_from_pro_table", "examples", "tmp_out",
                     (symbolMap, regionCharWordPROTable), suffix='_math_regions')

    ##################################
    # Get CCs for generated regions
    ##################################
    # Merge CCs, chars and words into one complete PRO table
    ccGrownProTable = get_cc_objects("examples", "A00-3007", regionWordBBs,
                                     "FR")
    mergedTable = merge_pro_tables(regionCharWordPROTable,
                                   ccGrownProTable)

    ##################################
    # Filter regions and objects
    ##################################
    regionTests = [

        min_max_object_test(2, 60, ['c', 'S']),
        max_height_test(256),  # 256 = 1in @256 dpi (MAGIC)
        in_dictionary(['en_GB', 'en_US'], remove_alpha=True,
                      report=False)
    ]

    # RZ -- allow only CCs to be extracted (later ops have no effect)

    objTypeFilter = obj_keep_types(['c', 'S'])  # , 'S'

    filteredPROTable = \
        filter_pr_table(combine_and_tests(regionTests), objTypeFilter,
                        mergedTable)

    ##################################
    # Align CCs and PDF Symbols
    # where possible -- *heuristic
    ##################################
    # Parser assumes that input regions are cropped around black pixels
    primitiveModPRO = filteredPROTable

    transform_sym_cc = rectify_container_members('S', 'c',
                                                 threshold=INTERSECTION_THRESHOLD, crop=True)
    primitiveModPRO = transform_pr_table(transform_sym_cc, None,
                                         filteredPROTable)

    cropTransform = crop_contents(png_width, png_height)
    finalPROTable = \
        transform_pr_table(cropTransform, None, primitiveModPRO)

    ##################################
    # Report, write to disk as TSV
    ##################################

    write_it_pro_tsv("final_pro", "examples", "tmp_out",
                     (symbolMap, finalPROTable))

    final_pro = open(os.path.join("tmp_out", "final_pro.tsv"))
    truth_pro = open(os.path.join("examples", "A00-3007.tsv"))

    final_pro_lines = final_pro.readlines()
    truth_pro_lines = truth_pro.readlines()

    # remove file name which should differ
    final_pro_lines.pop(0)
    truth_pro_lines.pop(0)

    # all other lines are the same
    for final_line, truth_line in zip(final_pro_lines, truth_pro_lines):
        assert final_line.strip() == truth_line.strip()


@pytest.fixture(scope="session", autouse=True)
def cleanup():
    os.makedirs("tmp_out", exist_ok=True)
    yield
    #shutil.rmtree("tmp_out")
