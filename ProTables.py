################################################################
#
# File I/O Routines for the SymbolScraper Pipeline
#
#
# R. Zanibbi, Aug 2021
# Modified by Ayush Kumar Shah, Sep 2023
################################################################

# RZ: Add debug operations
from debug_fns import *
import debug_fns
dTimer = DebugTimer("ChemScraper pipeline")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

from tqdm import tqdm
import json
import logging
import math
import os
import os.path as path
import os.path as osp
import re
import string
import time
import xml.etree.ElementTree as ET
from typing import Dict, List
from functools import reduce
from glob import glob
from itertools import chain, repeat
from operator import itemgetter
from collections import defaultdict

import cv2
# Access to dictionaries from OpenOffice
import enchant
# RZ adding this library to read image sizes fast
import imagesize
import numpy as np
from bs4 import BeautifulSoup as Soup
from intervaltree import Interval, IntervalTree
# For chem extraction
from shapely.geometry import Point, LineString, Polygon

from createHTML import create_mainHTML, create_pdfHTML

# AKS: Improve merging
# import rtree
from dprl_map_reduce import map_concat, parallel_progress
from util import get_cc_objects, CC_GRAY_THRESHOLD, REGION, OBJECT, DOCUMENT

import sqlitedict
from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

################################################################
# Debugging and visualization routines
################################################################

'''
    Simple interactive debugging function taking a variable,
    message string, and optional flag to indicate whether
    to pause for a keystroke.
'''

OBJ_REGION_THRESHOLD = 0.6  # Area coverage threshold, as percentage
INTERSECTION_THRESHOLD = 0.8  # Area coverage threshold, as percentage
INTERSECTION_THRESHOLD_THIN = 0.4  # Area coverage threshold, as percentage
sort_order_map = {"xy": itemgetter(0, 1), "yx": itemgetter(1, 0)}


def p_check(ref, msg="", pause=True):
    # Simple interactive debugging routine.
    if msg != "":
        print(">>> " + msg)
    print(ref)

    if pause:
        input(">> Hit enter to continue")


'''
    Display page list, each containing a list of bboxes.scanning/
    Pauses for input; set third arg to prevent pausing.
'''


def show_bb_table(page_bb_table, title, compact=True, pause=True):
    print("\n" + title)

    page_num = 0
    for page in page_bb_table:
        page_num += 1
        print("Page: " + str(page_num) + " Region BBs: " + str(len(page)))

        if not compact:
            for box in page:
                print("  ", end="")
                print(str(box))

    if pause:
        input("Hit Enter to continue.")


'''
    Display page list, each containing a list of pairs
    with region BBs and object BBs.
    Pauses for input; set third arg to prevent pausing.
'''


def show_pro_table(pro_table, title, compact=True, pause=False):
    out_string = "\n" + title + "\n"

    page_num = 0
    for page in pro_table:
        page_num += 1
        out_string += "\nPage: " + str(page_num) + " Regions: " + str(len(page)) + "\n"
        for region in page:
            if compact:
                out_string += "  (" + str(len(region[1])) + \
                              " Objs) [[ Type: " + str(region[2]) + \
                              "  Location: " + str(region[0]) + " ]]" + "\n"
            else:
                out_string += "  [[ Type: " + str(region[2]) + \
                              "  Location: " + str(region[0]) + " ]]" + "\n"

                out_string += "  >> " + str(region[1]) + "\n"

    # RZ: Trying to make output 'atomic' (i.e., hoping it won't be interrupted
    # in terminal output)
    print(out_string)

    if pause:
        input("Hit Enter to continue.")


def pro_summary(pro_table, title):
    out_string = "  " + title + " PRO: "

    page_num = len(pro_table)
    region_count = 0
    obj_count = 0
    o_type_counts = {}

    # Accumulate counts
    for p in range(len(pro_table)):
        regions = len(pro_table[p])
        region_count += regions

        for r in range(regions):
            objects = len(pro_table[p][r][REGION.OBJECTS.value])
            obj_count += objects

            for o in range(objects):
                object_entry = pro_table[p][r][REGION.OBJECTS.value][o]
                o_type = object_entry[OBJECT.TYPE.value]
                if o_type in o_type_counts.keys():
                    o_type_counts[o_type] += 1
                else:
                    o_type_counts[o_type] = 1

    # Generate output string
    out_string += f"{str(page_num)} pages, {str(region_count)} regions, {str(obj_count)} objects"
    out_string += "\n    " + str(o_type_counts)

    return out_string


################################################################
# Generating and Combining Bounding Boxes
################################################################

'''
    Returns # pages and list (page level) of bounding boxes on each page \
'''


def read_sscraper_xml(output_dir, filename, png_dir): #, png_width, png_height):
    # Parse the SymbolScraper data

    # Try to read the SymbolScraper file.
    sscraper_file = os.path.join(output_dir, filename) + ".xml"
    try:
        fp = open(sscraper_file, 'r', encoding='utf8')
    except Exception as e:

        logging.debug("      potentially empty file")
        logging.debug("      >> Error: XML for {} not parseable".format(filename))
        return 0, [], []

    # Start reading characters from pages.
    with fp:
        # Parse for pages and page size (assumes all pages same size!)
        return read_sscraper_xml_from_io(fp, filename, png_dir) # png_width, png_height)

def read_sscraper_json(output_dir, filename, png_dir, extract_graphics=False,
                       parallel=True, png_ext="png", doc_dir_mode=True,
                       width_adjustment=0.5, remove_manual_lines=True):
    # Parse the SymbolScraper data

    # Try to read the SymbolScraper file.
    sscraper_file = os.path.join(output_dir, filename) + ".json"
    try:
        fp = open(sscraper_file, 'r', encoding='utf8')
    except Exception as e:
        logging.debug("      potentially empty file")
        logging.debug("      >> Error: JSON for {} not parseable".format(filename))
        return 0, [], [], [], [], [], []

    # Start reading characters from pages.
    with fp:
        # Parse for pages and page size (assumes all pages same size!)
        return read_sscraper_json_from_io(fp, filename, png_dir,
                                          extract_graphics, parallel=parallel,
                                          png_ext=png_ext, doc_dir_mode=doc_dir_mode,
                                          width_adjustment=width_adjustment,
                                          remove_manual_lines=remove_manual_lines)

def read_word_from_json_obj(textLines, png_width, pdf_width, png_height,
                           pdf_height, p, filename, char_symbol='S',
                           word_symbol='W'):
    page_char_bb, page_word_bb = [], []
    for line in textLines:
        for word in line["words"]:
            # Initialize word BB to values too big/too small
            word_bb = [10000000, 10000000, -1, -1]
            word_string = ''

            for character in word["constituents"]:
                # import pdb; pdb.set_trace()
                shapely_object = {}
                bbox_dict = character['BBOX']
                symbol = character["value"]
                if symbol is None:
                    logging.debug("  >> (A) Warning: skipping SScraper null value for symbol in "
                                  + filename + ' page ' + str(p + 1) + ':\n     ' + str(character))
                    continue

                bbox = [bbox_dict['x'], bbox_dict['y'], bbox_dict['width'],
                        bbox_dict['height']]
                
                bbox = list(map(float, bbox))

                # Skip malformed boxes
                if True in np.isnan(bbox):
                    logging.debug("  >> (A) Warning: skipping SScraper (nan) bbox for "
                                  + filename + ' page ' + str(p + 1) + ':\n     ' +
                                  str(bbox))
                    continue

                # Convert sscraper coords to scanssd (image) coords;
                # Includes moving origin from bottom-left to top-left (x,y)
                min_x, min_y, max_x, max_y = scale_pdf2png(bbox, png_width, png_height, 
                                                           pdf_width, pdf_height)
                # Skip malformed boxes
                if True in np.isnan(bbox):
                    logging.debug("  >> (A) Warning: skipping SScraper (nan) bbox for "
                                  + filename + ' page ' + str(p + 1) + ':\n     ' +
                                  str(bbox))
                    continue
                shapely_object["box"] = [min_x, min_y, max_x, max_y]

                # Append the type of the object entry ((S)ymbol at the end).
                # Encode location, value/label, type, in that order.
                # conv_bb = list(map(round, [min_x, min_y, max_x, max_y]))
                conv_bb = [math.floor(min_x), math.floor(min_y), 
                           math.ceil(max_x), math.ceil(max_y)]
                conv_bb.extend([symbol, char_symbol])

                # Update word bounding box, round coordinates.
                # Capture the sequence of characters in the word.
                word_string += symbol
                word_bb = [
                    min(word_bb[0], conv_bb[0]), min(word_bb[1], conv_bb[1]),
                    max(word_bb[2], conv_bb[2]), max(word_bb[3], conv_bb[3]),
                    word_string, word_symbol]

                # AKS: Chemscrpaer Begin (From m/c/onepdf_test/extract_graphics_pdf)
                char_poly_coords = np.array([[min_x, min_y], [max_x, min_y], [max_x, max_y], 
                                             [min_x, max_y], [min_x, min_y]], dtype=float)
                # char_poly = Polygon(char_poly_coords)
                shapely_object["polygon"] = Polygon(char_poly_coords)
                # char_polys.append(char_poly)
                # AKS: Chemscrpaer End
                conv_bb.append(shapely_object)
                # AKS: To make equal size as geometry
                conv_bb.append([0])
                # Store bounding box as a list (in a list of lists )
                page_char_bb.append(conv_bb)

            if word_string != '':
                page_word_bb.append(word_bb)

        # AKS: Chemscrpaer Begin (From m/c/onepdf_test/extract_graphics_pdf)
        # char_polys = np.array(char_polys, dtype=object)
        # AKS: Chemscrpaer End
    return page_char_bb, page_word_bb

def get_ordered_rect_coords(rect):
    if rect.geom_type == 'LineString':
        return (Point(rect.coords[0]), Point(rect.coords[0]),
                Point(rect.coords[1]), Point(rect.coords[1]))
    elif rect.geom_type == 'Point':
        assert(False)

    last = None
    dists = []

    for p in rect.exterior.coords:
        p = Point(p)
        if last is not None:
            dists.append(last.distance(p))
        last = p

    dists = np.flatnonzero(np.array(dists) == np.min(dists)) % 4

    if len(dists) == 1:
        dists = np.array([dists[0].item(), (dists[0] + 2) % 4])
    
    return (Point(rect.exterior.coords[dists[0].item()]), Point(rect.exterior.coords[dists[0].item() + 1]),
            Point(rect.exterior.coords[dists[1].item()]), Point(rect.exterior.coords[dists[1].item() + 1]))


def poly_to_line(poly):
    # approximate a polygon meant to represent a line in the source pdf as a line
    
    rect = poly.minimum_rotated_rectangle
    (p1, p2, p3, p4) = get_ordered_rect_coords(rect)

    m1 = LineString([p1, p2]).centroid
    m2 = LineString([p3, p4]).centroid

    return LineString([m1, m2])

def read_graphics_from_json_obj(geometry_on_page, png_width, pdf_width, png_height,
                               pdf_height, p, filename, graphic_symbol='G',
                               geometry_symbol='GC', remove_manual_lines=True,
                                width_adjustment=0.5):
    page_graphic_bb, page_geometry_bb = [], []
    width_ratio = png_width / pdf_width
    height_ratio = png_height / pdf_height
    # Function to scale and move origin for individual point
    scale_points = lambda point: [point[0] * width_ratio,
                                 (pdf_height - point[1]) * height_ratio]
    conv_bb_idx = 0
    # Loop through all geometry collections
    for geometry in geometry_on_page:
        graphic_indices = []
        geometry_string = []
        # Get and scale Bounding Box of the GeometryCollection
        geometry_bbox_dict = geometry["BBOX"]
        geometry_bb = [geometry_bbox_dict['x'], geometry_bbox_dict['y'], 
                      geometry_bbox_dict['width'], geometry_bbox_dict['height']]

        # NOTE: From AD: one_pdf_test.py:extract_graphics_pdf: 
        # Check here if there are manual drawing context lines and remove them
        # TODO: Better condition?
        if remove_manual_lines and (((geometry_bb[2] / pdf_width) >= 0.5) or\
            ((geometry_bb[3] / pdf_height) >= 0.5)):
            # print("Manual drawing line removed")
            continue

        # geometry_bb = [10000000, 10000000, -1, -1]
        geometry_bb = list(map(float, geometry_bb))
        geometry_bb = list(scale_pdf2png(geometry_bb, png_width, png_height, 
                                   pdf_width, pdf_height))
        
        # Annotate other information for GeometryCollection
        geometry_shapely_obj = defaultdict(list)
        geometry_shapely_obj.update({key: geometry[key] for key in 
                                   (geometry.keys() - {"GeometryCollection", "BBOX"})})
        isLine = geometry["isLine"]
        isApproxLine = geometry.get("approxLine", None)
        # If geometry collection is a line
        if isLine:
            if geometry.get("approxLine", None):
                # Update geometry collection object by its approxline properties
                geometry_shapely_obj.update(geometry["approxLine"])
            else:
                # Case: If geometry collections has only 1 line
                # Update geometry collection object by its line properties
                constituents = geometry["GeometryCollection"]
                assert(len(constituents) == 1)
                constituents[0]["length"] = constituents[0]["length"] * math.sqrt(width_ratio**2 + height_ratio**2) / math.sqrt(2)
                constituents[0]["lineWidth"] = constituents[0]["lineWidth"] * math.sqrt(width_ratio**2 + height_ratio**2) / math.sqrt(2)
                geometry_shapely_obj.update(constituents[0])

            # Correct angle:
            geometry_angle = geometry_shapely_obj.get("angle", None)
            if geometry_angle:
                if geometry_angle == 360:
                    geometry_angle = 0
                elif geometry_angle >= 180:
                    geometry_angle -= 180
                geometry_shapely_obj["angle"] = geometry_angle

            # Scale and annotate points and the corresponding shapely object
            geometry_points_dict = geometry_shapely_obj.get("points", None)
            assert(geometry_points_dict)
            assert(len(geometry_points_dict) == 2)
            geometry_points = [[g_point["x"], g_point["y"]] for
                             g_point in geometry_points_dict]
            geometry_points = list(chain(*geometry_points))
            x_1, y_1, x_2, y_2 = scale_pdf2png(geometry_points, png_width, png_height, 
                                               pdf_width, pdf_height, difference=False)
            geometry_shapely_obj["points"] = [[x_1, y_1], [x_2, y_2]]
            if not isApproxLine:
                geometry_shapely_obj["linestring"] = LineString([[x_1, y_1], [x_2, y_2]])
            geometry_shapely_obj["length"] = geometry_shapely_obj["length"] * math.sqrt(width_ratio**2 + height_ratio**2) / math.sqrt(2)

        if isApproxLine:
            geometry_shapely_obj.pop("points")

        # For geometry collection that is not a line
        # Can be a collection of lines, or mixed graphics, we collect
        # information from its constituents

        for graphic in geometry["GeometryCollection"]:
            symbol = graphic["typeFromPDF"]
            graphic_shapely_object = graphic
            if symbol is None:
                logging.debug("  >> (A) Warning: skipping SScraper null value for symbol in "
                              + filename + ' page ' + str(p + 1) + ':\n     ' + str(graphic))
                continue

            # Annotate graphic points
            graphic_points_dict = graphic.get("points", None)
            graphic_bezier_points_dict = graphic.get("bezierPoints", None)
            assert(graphic_points_dict is not None)
            graphic_points = [[g_point["x"], g_point["y"]] for
                             g_point in graphic_points_dict]
            graphic_points_unscaled = graphic_points.copy()
            graphic_points = list(map(scale_points, graphic_points))

            if graphic_bezier_points_dict:
                # Since available as p1: {}, p2: {}
                graphic_bezier_points = [[g_point[1]["x"], g_point[1]["y"]] for
                                             g_point in sorted(graphic_bezier_points_dict.items(),
                                                 key=lambda x: int(x[0][1:]))]
                graphic_bezier_points = list(map(scale_points, graphic_bezier_points))

            # Construct graphic bounding box from points, round coordinates.
            graphic_points_array = np.array(graphic_points_unscaled).astype(float)
            # TODO: Correct the linewidth adjustment, 0.5 or 0.15 for now. Or use
            # geometry bbox coordinates somehow (maybe calculation in sscraper)
            bbox_min = graphic_points_array.min(axis=0) - (width_adjustment * graphic.get("lineWidth", 0))
            bbox_max = graphic_points_array.max(axis=0) + (width_adjustment * graphic.get("lineWidth", 0))
            # conv_bb = np.concatenate((bbox_min, bbox_max)).tolist()
            conv_bb = [bbox_min, bbox_max]
            # import pdb; pdb.set_trace()
            conv_bb = list(chain(*list(map(scale_points, conv_bb))))
            graphic_shapely_object["box"] = conv_bb.copy()
            conv_bb[:2] = list(map(math.floor, conv_bb[:2]))
            conv_bb[2:] = list(map(math.ceil, conv_bb[2:]))
            
            # conv_bb[2:] = np.ceil(conv_bb[2:])
            # conv_bb = conv_bb.astype(int).tolist()
            # Skip malformed boxes
            if True in np.isnan(conv_bb):
                logging.debug("  >> (A) Warning: skipping SScraper (nan) bbox for "
                              + filename + ' page ' + str(p + 1) + ':\n     ' +
                              str(conv_bb))
                continue

            # Encode location, value/label, type, in that order.
            # Append the type of the object entry ((S)ymbol at the end).
            conv_bb.extend([symbol, graphic_symbol])



            graphic_angle = graphic.get("angle", None)
            if graphic_angle:
                if graphic_angle == 360:
                    graphic_angle = 0
                elif graphic_angle >= 180:
                    graphic_angle -= 180
                graphic_shapely_object["angle"] = graphic_angle
            # Annotate other information for Graphic in GeometryCollection
            graphic_shapely_object["points"] = graphic_points.copy()
            graphic_length = graphic.get("length", None)
            if graphic_length:
                graphic_shapely_object["length"] = graphic_length * math.sqrt(width_ratio**2 + height_ratio**2) / math.sqrt(2)

            graphic_lineWidth = graphic.get("lineWidth", None)
            if graphic_lineWidth:
                if not isLine:
                    graphic_shapely_object["lineWidth"] = graphic_lineWidth * math.sqrt(width_ratio**2 + height_ratio**2) / math.sqrt(2)
                else:
                    graphic_shapely_object["lineWidth"] = graphic_lineWidth
            # For curves
            if graphic_bezier_points_dict:
                graphic_shapely_object["bezierPoints"] = graphic_bezier_points.copy()
            if symbol == "rectangle":
                assert(len(graphic_points) == 2)
                x_1, y_1, x_2, y_2 = list(chain(*graphic_points))
                graphic_poly_coords = np.array([[x_1, y_1], [x_2, y_1], [x_2, y_2], 
                                                 [x_1, y_2], [x_1, y_1]], dtype=float)
                graphic_shapely_object["points"] = graphic_poly_coords.tolist()
                graphic_shapely_object["polygon"] = Polygon(graphic_poly_coords)
            else:
                # Line or curve
                graphic_shapely_object["linestring"] = LineString(graphic_points)
            conv_bb.append(graphic_shapely_object)

            # Store bounding box as a list (in a list of lists )
            graphic_indices.append(conv_bb_idx)
            conv_bb_idx += 1
            page_graphic_bb.append(conv_bb)

            # Capture the sequence of graphics in the geometry collection.
            geometry_string.append(symbol)
            if (not isLine) or isApproxLine:
                graphics_keys = {"points", "angle", "length", "lineWidth",
                                 "curvedTo", "lengthSegments", "bezierPoints"}
                for key in graphics_keys:
                    if isApproxLine and (key == 'angle' or key == 'lineWidth' or key == 'length'):
                        continue 
                    if key == "points" or key == "bezierPoints" or key == "lengthSements":
                        geometry_shapely_obj[key].extend(graphic_shapely_object.get(key, []))
                    else:
                        geometry_shapely_obj[key].append(graphic_shapely_object.get(key, None))

            # geometry_bb = [
            #         min(geometry_bb[0], conv_bb[0]), min(geometry_bb[1], conv_bb[1]),
            #         max(geometry_bb[2], conv_bb[2]), max(geometry_bb[3], conv_bb[3]),
            #         geometry_string, geometry_symbol]

        # Update Shapely object for GeometryCollection
        geometry_points = geometry_shapely_obj["points"]

        if isApproxLine:
            poly = Polygon(geometry_points)
            geometry_shapely_obj["linestring"] = poly_to_line(poly)
            (p1, p2, _, _) = get_ordered_rect_coords(poly.minimum_rotated_rectangle)
            geometry_shapely_obj["lineWidth"] = p1.distance(p2)
            geometry_shapely_obj["polygon"] = poly.minimum_rotated_rectangle
            # geometry_shapely_obj["polygon"] = poly

        if not isLine:
            if "curve" in geometry_string:
                # Use linstring for curves
                geometry_shapely_obj["linestring"] = LineString(geometry_points)
                geometry_shapely_obj["polygon"] = Polygon(geometry_shapely_obj["bezierPoints"])
            if len(geometry_points) > 2:
                geometry_shapely_obj["polygon"] = Polygon(geometry_points)

        geometry_shapely_obj['box'] = geometry_bb[:4]
        # Round off GeometryCollection bboxes
        geometry_bb[:2] = list(map(math.floor, geometry_bb[:2]))
        geometry_bb[2:4] = list(map(math.ceil, geometry_bb[2:4]))
        geometry_bb = [*geometry_bb, geometry_string, geometry_symbol,
                       geometry_shapely_obj]
        geometry_bb[4] = " ".join(geometry_bb[4])
        if geometry_string != '':
            page_geometry_bb.append(geometry_bb)
        # Add graphic indices
        geometry_bb.append(graphic_indices)

    return page_graphic_bb, page_geometry_bb

def read_sscraper_json_page(params): # png_width, png_height):
    p, page, filename, png_dir, extract_graphics, \
        page_size_map, png_ext, doc_dir_mode, \
        width_adjustment, remove_manual_lines = params
    page_graphic_bbs = []
    page_geometry_bbs = []
    page_char_geometry_bbs = []

    # Get number of pages from top of file, less likely to cause
    # inaccurate counts in corrupted files.
    page_bbox = page["BBOX"]
    pdf_height = page_bbox["height"] - page_bbox["x"]
    pdf_width = page_bbox["width"] - page_bbox["y"]

    textLines = page["text"]["textLines"]
    # words_on_page = list(map(itemgetter("words"), textLines))
    if png_dir:
        if doc_dir_mode:
            png_file = os.path.join(png_dir, str(p+1) + "." + png_ext)
        else:
            png_file = png_dir + "." + png_ext
        ( png_width, png_height ) = imagesize.get( png_file)
    else:
        assert(page_size_map), "Pdf page image info not found"
        png_height = page_size_map[str(p)][0]
        png_width = page_size_map[str(p)][1]

    page_sizes = ( png_width, png_height )
    
    page_char_bbs, page_word_bbs = read_word_from_json_obj(textLines, png_width, pdf_width, 
                                                         png_height, pdf_height, p, filename, 
                                                          char_symbol='S', word_symbol='W')
    if extract_graphics:
        geometry_on_page = page["graphics"]
        page_graphic_bbs, page_geometry_bbs = \
            read_graphics_from_json_obj(geometry_on_page, png_width, pdf_width, 
                                        png_height, pdf_height, p, filename, 
                                        graphic_symbol='G', geometry_symbol='GC', 
                                        width_adjustment=width_adjustment, 
                                        remove_manual_lines=remove_manual_lines)
        page_char_geometry_bbs = page_char_bbs + page_geometry_bbs

    # Return list of list so that map_concat concatenates lists and not
    # elements making pages separable later
    return [[page_sizes, page_char_bbs, page_word_bbs,
           page_graphic_bbs, page_geometry_bbs, page_char_geometry_bbs]]

def read_sscraper_json_from_io(io, filename, png_dir, extract_graphics=False,
                               page_size_map: Dict[str, List] = None,
                               parallel=True, png_ext="png", doc_dir_mode=True,
                               width_adjustment=0.5, remove_manual_lines=True):
    data = json.load(io)
    pages = data["pages"]
    num_pages = len(pages)
    page_sizes = [ None ] * num_pages   # Use None to capture errors
    page_char_bbs = [[] for _ in range(num_pages)]
    page_word_bbs = [[] for _ in range(num_pages)]
    page_graphic_bbs = [[] for _ in range(num_pages)]
    page_geometry_bbs = [[] for _ in range(num_pages)]
    page_char_geometry_bbs = [[] for _ in range(num_pages)]
    
    # AKS: Parallelize read across pages
    if parallel:
        input_list = list(zip(range(len(pages)), pages, repeat(filename),
                              repeat(png_dir), repeat(extract_graphics),
                              repeat(page_size_map), repeat(png_ext), 
                              repeat(doc_dir_mode), repeat(width_adjustment),
                              repeat(remove_manual_lines)))
        pages_info = map_concat(read_sscraper_json_page, input_list) 
        for p, page_info in enumerate(pages_info):
            page_sizes[p] = page_info[0]
            page_char_bbs[p] = page_info[1]
            page_word_bbs[p] = page_info[2]
            page_graphic_bbs[p] = page_info[3]
            page_geometry_bbs[p] = page_info[4]
            page_char_geometry_bbs[p] = page_info[5]
    else:
        for p, page in enumerate(pages):
            page_info = read_sscraper_json_page((p, page, filename, png_dir,
                                                extract_graphics, page_size_map,
                                                 png_ext, doc_dir_mode,
                                                 width_adjustment, remove_manual_lines))[0]
            page_sizes[p] = page_info[0]
            page_char_bbs[p] = page_info[1]
            page_word_bbs[p] = page_info[2]
            page_graphic_bbs[p] = page_info[3]
            page_geometry_bbs[p] = page_info[4]
            page_char_geometry_bbs[p] = page_info[5]

    # dTimer.check("Parallel read")
    return num_pages, page_sizes, page_char_bbs, page_word_bbs,\
            page_graphic_bbs, page_geometry_bbs, page_char_geometry_bbs

def scale_pdf2png(bbox, png_width, png_height, pdf_width, pdf_height,
                  difference=True):
    """
    Convert sscraper coords to scanssd (image) coords;
    Includes moving origin from bottom-left to top-left (x,y)
    difference: bool, True: the 3rd and 4th coordinates are width and height,
                      False: the 3rd and 4th coordinates are actual coordinates
    """
    
    if not difference:
        min_x = (bbox[0] * png_width / pdf_width)
        max_x = (bbox[2] * png_width / pdf_width)
        min_y = ((pdf_height - bbox[1]) * png_height / pdf_height)
        max_y = ((pdf_height - bbox[3]) * png_height / pdf_height)
    else:
        min_x = (bbox[0] * png_width / pdf_width)
        max_x = ((bbox[0] + bbox[2]) * png_width / pdf_width)
        min_y = ((pdf_height - bbox[1] - bbox[3])
                 * png_height / pdf_height)
        max_y = ((pdf_height - bbox[1]) * png_height / pdf_height)
    return min_x, min_y, max_x, max_y

def read_word_from_obj(words_on_page, png_width, pdf_width, png_height,
                       pdf_height, p, filename, char='Char', char_symbol='S',
                       word_symbol='W'):
    page_char_bb, page_word_bb = [], []
    for word in words_on_page:
        characters_in_word = word.find_all(char)
        if len(characters_in_word) < 1:
            continue

        # Initialize word BB to values too big/too small
        word_bb = [10000000, 10000000, -1, -1]
        word_string = ''

        for character in characters_in_word:
            # if char == "Graphic":
                # import pdb; pdb.set_trace()
            bbox_string_list = character['BBOX'].split(" ")
            symbol = character.string
            if symbol is None and character.has_attr('value'):
                symbol = character['value']
            elif symbol is None:
                logging.debug("  >> (A) Warning: skipping SScraper null value for symbol in "
                              + filename + ' page ' + str(p + 1) + ':\n     ' +
                              str(character))
                continue

            bbox = [float(c) for c in bbox_string_list]

            # Skip malformed boxes
            if True in np.isnan(bbox):
                logging.debug("  >> (A) Warning: skipping SScraper (nan) bbox for "
                              + filename + ' page ' + str(p + 1) + ':\n     ' +
                              str(bbox))
                continue

            # Convert sscraper coords to scanssd (image) coords;
            # Includes moving origin from bottom-left to top-left (x,y)
            min_x, min_y, max_x, max_y = scale_pdf2png(bbox, png_width, png_height, 
                                                       pdf_width, pdf_height)

            if True in np.isnan([min_x, min_y, max_x, max_y]):
                logging.debug("  >> (B) Warning: skipping SScraper (nan) bbox for "
                              + filename + ' page ' + str(p + 1) + ':\n     ' +
                              str(bbox))
                continue

            # Append the type of the object entry ((S)ymbol at the end).
            # Encode location, value/label, type, in that order.
            conv_bb = list(map(round, [min_x, min_y, max_x, max_y]))
            # conv_bb = [math.floor(min_x), math.floor(min_y), 
            #            math.ceil(max_x), math.ceil(max_y)]
            conv_bb.extend([symbol, char_symbol])

            # Update word bounding box, round coordinates.
            # Capture the sequence of characters in the word.
            word_string += symbol
            word_bb = [
                min(word_bb[0], conv_bb[0]), min(word_bb[1], conv_bb[1]),
                max(word_bb[2], conv_bb[2]), max(word_bb[3], conv_bb[3]),
                word_string, word_symbol]

            # Store bounding box as a list (in a list of lists )
            page_char_bb.append(conv_bb)


        if word_string != '':
            page_word_bb.append(word_bb)

    return page_char_bb, page_word_bb

def read_sscraper_xml_from_io(io, filename, png_dir): # png_width, png_height):
    symbol_soup = Soup(io, "xml")
    # pages_wrapper = symbol_soup('Pages')[0]

    # Create a page list, each item referring to char bboxes on that page.
    # Note: The SymbolScraper coordinates are (long!) floats.
    pages = symbol_soup('Page')
    num_pages = len(pages)
    page_sizes = [ None ] * num_pages   # Use None to capture errors
    page_char_bbs = [[] for _ in range(num_pages)]
    page_word_bbs = [[] for _ in range(num_pages)]

    for p in range(0, num_pages):
        # RZ: DEBUG: collect page size on each page.
        page_bounding_box = pages[p]['BBOX'].split(" ")
        page_bounding_box = [float(c) for c in page_bounding_box]

        # Get number of pages from top of file, less likely to cause
        # inaccurate counts in corrupted files.
        pdf_width = page_bounding_box[2] - page_bounding_box[0]
        pdf_height = page_bounding_box[3] - page_bounding_box[1]

        words_on_page = pages[p].find_all('Word')

        ( png_width, png_height ) = \
            imagesize.get( png_dir + "/" + str(p+1) + ".png" )
        page_sizes[p] = ( png_width, png_height )
        
        # DEBUG
        #print("Page sizes")
        #print(page_sizes)
        #print("Page size (" + str(p) + ")")
        #print( str(png_width) + " " + str(png_height) + "(w/h) " + str(num_pages) + " pages.")
        #_ = input()

        page_char_bbs[p], page_word_bbs[p]  = read_word_from_obj(words_on_page, png_width, pdf_width, png_height,
                                                                   pdf_height, p, filename, char='Char', 
                                                                  char_symbol='S', word_symbol='W')

    return num_pages, page_sizes, page_char_bbs, page_word_bbs
            

################################################################
# Reading and writing TSV and CSV
################################################################
'''
    Read regions, return list of pages, each containing a list of
    detected formula regions, each as a (minX, minY, maxX, maxY) tuple.
'''


def read_region_csv(region_file_path, num_pages):
    # logging.debug("  * Reading Math Regions from ScanSSD" )
    with open(region_file_path, "r") as csv_file:
        return read_region_csv_from_io(csv_file, num_pages)


def read_region_csv_from_io(io, num_pages):
    page_region_bbs = [[] for _ in range(num_pages)]
    num_regions = 0
    for line in io:  # for each equation
        bbox_string_list = line.strip("\n").split(",")
        (page, min_x, min_y, max_x, max_y) = [round(float(c)) for c in bbox_string_list]
        # if int(page) >= numPages or int(page) < 0:
        #    pcheck(str(page) + " page and " + str(numPages) + " given pages.")
        # if maxX > minX and maxY > minY:
        page_region_bbs[int(page)].append((min_x, min_y, max_x, max_y))
        # pcheck(page_region_bbs, "next region BB entry")
        num_regions += 1

    # logging.debug("    " + str(num_regions) + " formula regions")
    return page_region_bbs


def replaceSymbol(s, char_map):
    # Do a direct lookup for replacements.
    if s in char_map.keys():
        return char_map[s]
    return s


def round_bb_coords(bb_table):
    new_bb_table = [[] for _ in range(len(bb_table))]
    for p in range(len(bb_table)):
        new_bb_table = list(map(round, bb_table[p]))

    return new_bb_table


def round_pro_coords(pro_table):
    # Return a modified copy (leave input table as-is)
    new_table = [[] for _ in range(len(pro_table))]

    for p in range(len(pro_table)):
        new_table[p] = []
        for r in range(len(pro_table[p])):
            new_table[p].append(
                (tuple(map(round, pro_table[p][r][REGION.BBOX.value])),
                 [],
                 pro_table[p][r][REGION.TYPE.value]))

            for o in range(len(pro_table[p][r][REGION.OBJECTS.value])):
                (min_x, min_y, max_x, max_y) = pro_table[p][r][REGION.OBJECTS.value][o][:OBJECT.MAX_Y.value+1]
                o_values = pro_table[p][r][REGION.OBJECTS.value][o][OBJECT.MAX_Y.value+1:]
                new_table[p][-1][REGION.OBJECTS.value].append(
                    (round(min_x), round(min_y), round(max_x), round(max_y),
                     *o_values))
    return new_table


'''
    Given a list of (document, PRO Table) pairs, create a TSV PRO file.
'''


def write_pro_tsv(out_dir, doc_pro_pair_list, symbol_map, docs_page_sizes, 
                  suffix='', sort_objs=True, lg=False, page_files=None, page_dir=""):
    # WARNING: File names are based on the document name in passed list.
    # Use existing function - write documents to a passed directory.
    if page_files is None:
        page_files = []
        
    idx = 0
    for (docPath, docPRO) in doc_pro_pair_list:
        input_dir = os.path.abspath(os.path.dirname(docPath))
        # Get filename without extension (assumes only one .X)
        if not lg:
            filename = (os.path.basename(docPath)).split('.')[0]
            page_sizes = docs_page_sizes[idx][1]
        else:
            filename = os.path.basename(docPath)
            page_sizes = []

        # Use existing I/O function for individual file. 
        # Need to pass 'False' to use given paths
        # DEBUG
        #print("WRITE_PRO_TSV DPS [ " + str(idx) + "]: " 
        #        + str(docs_page_sizes[idx]) )
        write_it_pro_tsv(filename, 
            input_dir, out_dir, (symbol_map, docPRO), 
            page_sizes,
            False, suffix, sort_objs, page_files=page_files, 
            page_dir=page_dir)

        idx += 1

'''
    PRO Table writing for use in main pipeline calls.
'''


def write_it_pro_tsv(filename, input_dir, output_dir, region_pair, page_sizes,
                     use_globals=True, suffix='', sort_objs=True, page_files=None,
                     page_dir=""):
    # regionPair is ( labelMap, obj_region_table )

    # Define output path and input file
    if page_files is None:
        page_files = []

    tsv_output_path = os.path.abspath(osp.join(output_dir))
    doc_name = os.path.abspath(os.path.join(input_dir, "pdf", filename)) + ".pdf"

    # Define input/output differently if we want passed paths exactly
    if not use_globals:
        tsv_output_path = output_dir
        doc_name = os.path.abspath(os.path.join(input_dir,
                                                filename + ".pdf"))

    tsv_file = osp.join(tsv_output_path, filename + suffix) + ".tsv"

    # Create output TSV file
    if not osp.exists(tsv_output_path):
        os.makedirs(tsv_output_path)
    tsv_output = open(tsv_file, "w", encoding='utf8')

    ##################################
    # Write TSV contents for PRO
    ##################################
    # Get object label, label map for values, and region table
    # Sort table to insure outputs are ordered.
    (labelMap, obj_region_table) = region_pair
    obj_region_table = sort_page_regions(obj_region_table, sort_objs)

    # Write document entry
    doc_id = 1
    doc_entry = "D\t" + str(doc_id) + "\t" + doc_name + "\n"
    tsv_output.write(doc_entry)

    # Page
    #DEBUG
    #print( "Regions Pages: {}  Physical Pages: {}".format(len(obj_region_table), len(page_sizes)) )

    for p in range(len(obj_region_table)):
        page_name = ""
        # Write file name for each page if available
        if page_files:
            page_name = os.path.abspath(os.path.join(page_dir,
                                                     page_files[p]))
            tsv_output.write("P\t{}\t{}\n".format(p + 1, page_name))
        else:
            tsv_output.write("P\t{}\t{}\t{}\n".format(p + 1, \
                page_sizes[p][0], page_sizes[p][1]))


        # Region
        for r in range(len(obj_region_table[p])):
            (minX, minY, maxX, maxY) = obj_region_table[p][r][REGION.BBOX.value]
            rtype = obj_region_table[p][r][REGION.TYPE.value]
            tsv_output.write("{}\t{}\t{:.0f}\t{:.0f}\t{:.0f}\t{:.0f}\n".
                             format(rtype, r + 1, round(minX), round(minY),
                                    round(maxX), round(maxY)))

            # Object
            for o in range(len(obj_region_table[p][r][REGION.OBJECTS.value])):
                info = obj_region_table[p][r][REGION.OBJECTS.value][o]
                instance, symtag = "", ""
                if len(info) == 9:  # CROHME data has traces as 9th value
                    # entries with BBs, values and types
                    (minX, minY, maxX, maxY, label, otype, instance, symtag, _) = \
                        info
                elif len(info) == 8:  # INFTY data has 8 values, traces absent
                    (minX, minY, maxX, maxY, label, otype, instance, symtag) = \
                        info
                else:
                    (minX, minY, maxX, maxY, label, otype) = info

                tsv_output.write(("{}\t" * 3 + "{:.0f}\t" * 4 + "{}\t{}\n").
                                 format(otype, o + 1, replaceSymbol(label, labelMap),
                                        round(minX), round(minY), round(maxX),
                                        round(maxY), instance, symtag))

    tsv_output.close()

def convBB(num_list):
    f_list = map(float, num_list)
    return list(map(round, f_list))

# sort contours from left-right, top-down
def sort_contours(contours):
    contours = sorted(contours.items(),
                      key=lambda x:tuple(np.concatenate(x[1]).min(axis=0)[0]))
    contours_idx = list(map(itemgetter(0), contours))
    return contours_idx

# sort CC boxes from left-right, top-down
def sort_CCs(boxes):
    boxes = sorted(boxes.items(), key=lambda x:x[1][0])
    boxes = sorted(boxes, key=lambda x:x[1][1])
    box_idx = list(map(itemgetter(0), boxes))
    return box_idx

def tsv2pro(tsv_dir, img_dir):
    # AKS: Use glob to avoid hidden files
    tsv_files = sorted([path.basename(x) for x in
                        glob(path.join(tsv_dir, "*.tsv"))])

    # Create map from filenames to PRO table for each
    # document.
    pro_table_list = []
    region_triples = []
    docs_page_sizes = []

    curr_page = 0
    for t in range(len(tsv_files)):
        # Read in PRO table
        (pro_table, next_doc_page_sizes ) = read_pro_tsv(os.path.join(tsv_dir,
                                      tsv_files[t])) #[0][1]
        pro_table_list.append( pro_table[0][1])
        docs_page_sizes.append( next_doc_page_sizes[0] )
        #print("TSV2PRO next_doc_pag_sizes: " + str(next_doc_page_sizes))
        #print("TSV2PRO dps: " + str(docs_page_sizes))

        # Create indexing triples (doc, page, region)
        pages = len(pro_table_list[-1])
        for p in range(pages):
            # Add tuples to list.
            region_count = len(pro_table_list[-1][p])
            t_list = [t] * region_count
            plist = [p] * region_count
            tuples = list(zip(t_list, plist, list(range(region_count))))

            region_triples.extend(tuples)
    # Pipeline stores page images below directories for each PDF
    image_path = path.join(img_dir,
                           path.splitext(tsv_files[0])[0],
                           str(curr_page + 1) + ".png")
    # Image is inverted (*every time -- earlier there was a cond.)
    # '0' argument tells OpenCV to read as grayscale image
    curr_page_img = cv2.bitwise_not(cv2.imread(image_path, 0)) / 255.0
    lg = False

    return tsv_files, pro_table_list, region_triples, docs_page_sizes, curr_page_img, lg


def read_pro_tsv(file_path):
    """
        Read in a TSV table and return a list of (filename, PRO Table) pairs.
    """
    with open(file_path, "r", encoding='utf8') as tsv_file:
        return read_pro_tsv_from_io(tsv_file)


def read_pro_tsv_from_io(io):
    # RZ: HACK adding page size for each document as parallel data structure
    out_list = []
    docs_page_sizes = []
    # Read from TSV file
    for line in io:
        info = line.split('\t')
        entry_type = info[0]

        if entry_type == 'D':
            # Append ( filename, empty-PRO )
            out_list.append((info[2], []))
            docs_page_sizes.append((info[2].strip(), []))
        elif entry_type == 'P':
            # Add page to current PRO table
            # RZ: AND add page width and height to page size table
            out_list[-1][1].append([])
            if len(info) == 4:
                docs_page_sizes[-1][1].append( (int(info[2]), int(info[3])) )
        elif entry_type == 'FR':
            # Add region to final page in current PRO table
            out_list[-1][1][-1].append([tuple(convBB(info[2:6])),
                                        [], entry_type])
        else:
            # print(" >> EntryType: " + entry_type )
            # print("    Line: " + str(info ))
            # Writes bounding box, value, entry_type
            bbox = convBB(info[3:7])
            # print(info[2], bbox)
            out_list[-1][1][-1][-1][1].append(
                (bbox[0], bbox[1], bbox[2], bbox[3],
                 info[2], entry_type))

    #DEBUG
    #p_check( docs_page_sizes, "docs_page_sizes on read_pro_tsv_from_io out")
    return ( out_list, docs_page_sizes )


def lg2pro_sqlite(tsv_dir, img_dir, tsv_files, sqlite_db_path="pro_table.sqlite",
                   excludeFiles=None, mode="contours", inputs="primitives", 
                  from_lg_list=False, lg_list=None, server_mode=False):
    global CURR_COUNT, MAX_COUNT
    if excludeFiles:
        tsv_files = list(filter(lambda x: x not in excludeFiles, tsv_files))

    # sqlite_dict = init_sqlite_dict(sqlite_db_path)
    region_triples = []
    pro_table_list = []

    # Forcing all formula files / pages in 1 document using []
    for t in range(len([tsv_files])):
        # Read in PRO table
        input_pararms = [(tsv_dir, index, tsv_file, sqlite_db_path, False, mode,
                          from_lg_list, lg_list, inputs, server_mode) 
                             for index, tsv_file in enumerate(tsv_files)]
        MAX_COUNT = len(tsv_files)
        region_triples = map_concat(read_pro_lg_new_parallel, input_pararms)
        # with sqlitedict.SqliteDict(sqlite_db_path, flag='r') as db:
        #     pro_table_list.append(db)
        # pro_table_list.append(sqlitedict.SqliteDict(sqlite_db_path))

    image_path = None
    curr_page_img = None
    if not from_lg_list:
        image_path = os.path.join(img_dir, path.splitext(tsv_files[0])[0].\
                                        replace("_line", "").replace("_pdf", "") + ".PNG")
        if os.path.exists(image_path):
            curr_page_img = cv2.imread(image_path, 0) / 255.0
    lg = True
    return tsv_files, region_triples, curr_page_img, lg

def lg2pro(tsv_dir, img_dir, tsv_files, excludeFiles=None, mode="contours",
           inputs="primitives", from_lg_list=False, lg_list=None):
    if excludeFiles:
        tsv_files = list(filter(lambda x: x not in excludeFiles, tsv_files))
    # Create map from filenames to PRO table for each
    # document.
    pro_table_list = []
    region_triples = []

    # Forcing all formula files / pages in 1 document using []
    for t in range(len([tsv_files])):
        # Read in PRO table

        
        pro_table_list.append(read_pro_lg(tsv_dir, tsv_files, mode=mode, from_string=from_lg_list, lg_list=lg_list)[0][1])

        # Create indexing triples (doc, page, region)
        pages = len(pro_table_list[-1])
        for p in range(pages):
            # Add tuples to list.
            region_count = len(pro_table_list[-1][p])
            t_list = [t] * region_count
            p_list = [p] * region_count
            tuples = list(zip(t_list, p_list, list(range(region_count)), [0]))
            # If we want to use the gt symbols as addtional instances
            if inputs == "primitives_gtsymbols":
                segmentations = pro_table_list[-1][p][0][2]
                if segmentations:
                    tuples.append((*tuples[0][:3], 1))
                    # out_files.append(tsv_files[p])
            region_triples.extend(tuples)

    image_path = None
    curr_page_img = None
    if not from_lg_list:
        image_path = os.path.join(img_dir, path.splitext(tsv_files[0])[0].\
                                        replace("_line", "").replace("_pdf", "") + ".PNG")
        if os.path.exists(image_path):
            curr_page_img = cv2.imread(image_path, 0) / 255.0
    lg = True
    return tsv_files, pro_table_list, region_triples, curr_page_img, lg

def read_pro_lg_new_parallel(params):
    """
    Read LG files and store PRO Table results in an SQLite-backed dictionary,
    maintaining the original out_list structure.
    """
    global CURR_COUNT, MAX_COUNT
    file_dir, index, filePath, sqlite_db_path, get_relations,\
        mode, from_string, lg_list, inputs, server_mode = params

    db = sqlitedict.SqliteDict(sqlite_db_path, autocommit=True)
    region_triples = []
    if index in db:
        region_triples.append((0, index, 0, 0))
        out_list = db[index]
        if inputs == "primitives_gtsymbols" and out_list[-2]:
            region_triples.append((0, index, 0, 1))
        if not server_mode:
            parallel_progress(CURR_COUNT, MAX_COUNT,"lg files processed")
        db.close()
        return region_triples
    # sqlite_dict = init_sqlite_dict(sqlite_db_path)

    out_list = [(0, 0, 0, 0), [], False, 'FR']  # Initial region
    # out_list[-1][DOCUMENT.PROTABLE.value][-1].append([(0, 0, 0, 0), [], False, 'FR'])  # Initial region

    symLabels, comps, boxes, contours, comps_reverse = {}, {}, {}, {}, {}

    if from_string:
        tsvFile = lg_list[index].splitlines()
    else:
        with open(os.path.join(file_dir, filePath), "r") as data_file:
            tsvFile = data_file.read().splitlines()

    for line in tsvFile:
        info = [p.strip(",") for p in line.strip().split(" ")]
        entry_type = info[0].lower()

        if entry_type == "o":
            label = info[2]
            instance = info[1]
            symLabels[instance] = label
            comps[instance] = [int(x) for x in info[4:]]
            comps_reverse.update({x: instance for x in comps[instance]})

        elif entry_type == "#cc":
            instance = int(info[1])
            boxes[instance] = np.array(info[2:]).astype(int)

        elif entry_type == "#contours":
            instance = int(info[1])
            points = [[list(map(int, info[2:][i:i+2]))] for i in range(0, len(info[2:]), 2)]
            contours.setdefault(instance, []).append(points)

        # elif get_relations and entry_type == "r":
        #     relations.append((info[1], info[2], info[3]))

    instances = sort_contours(contours) if mode == "contours" else sort_CCs(boxes)

    sym_tags = []
    bboxes = []
    for cc in instances:
        key = comps_reverse.get(cc, None)
        if key is None:
            continue
        sym_tags.append(key)

        if mode == "contours":
            points = contours[cc]
            points_np = np.concatenate(points)
            bbox_min, bbox_max = points_np.min(axis=0)[0], points_np.max(axis=0)[0]
            bbox = [bbox_min[1], bbox_min[0], bbox_max[1], bbox_max[0]]
            out_list[REGION.OBJECTS.value].append(
                (bbox[1], bbox[0], bbox[3], bbox[2], symLabels[key], "c",
                 cc, key, points)
            )
        else:
            bbox = boxes[cc]
            out_list[REGION.OBJECTS.value].append(
                (bbox[1], bbox[0], bbox[3], bbox[2], symLabels[key], "c", cc, key)
            )
        bboxes.append(bbox)

    if bboxes:
        UL_y, UL_x = np.array(bboxes).min(axis=0)[:2].tolist()
        LR_y, LR_x = np.array(bboxes).max(axis=0)[2:].tolist()
        out_list[REGION.BBOX.value] = (UL_x, UL_y, LR_x, LR_y)

    out_list[-2] = len(sym_tags) != len(set(sym_tags))

    db[index] = [out_list]

    region_triples.append((0, index, 0, 0))
    if inputs == "primitives_gtsymbols" and out_list[-2]:
        region_triples.append((0, index, 0, 1))

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    if not server_mode:
        parallel_progress(CURR_COUNT, MAX_COUNT, "lg files processed")
    db.close()
    return region_triples

def read_pro_lg(file_dir, file_paths, get_relations=False, mode="contours",
                from_string=False, lg_list=None):
    """
        Read in list of LG files and return a list of (filename, PRO Table) pairs.
    """
    #Bryan added
    relations = []
    out_list = [("INFTY", [])]
    # AKS: Read from LG File
    # Append ( filename, empty-PRO )
    for index, filePath in enumerate(file_paths):
        # Add page to current PRO table
        out_list[-1][DOCUMENT.PROTABLE.value].append([])
        # Add region to final page in current PRO table
        out_list[-1][DOCUMENT.PROTABLE.value][-1].append([(0, 0, 0, 0), [], False, 'FR'])

        (symLabels, comps, boxes, contours, comps_reverse) = {}, {}, {}, {}, {}
        if from_string:
            tsvFile = lg_list[index].splitlines()
        else:
            with open(path.join(file_dir, filePath), "r") as data_file:
                tsvFile = data_file.read().splitlines()
        for line in tsvFile:
            info = [p.strip(",") for p in line.strip().split(" ")]
            entry_type = info[0].lower()

            if entry_type == "o":
                label = info[2]
                    # AKS: Convert id to int
                # instance = int(info[1])
                instance = info[1]
                symLabels[instance] = label
                # AKS: Convert id to int
                comps[instance] = [int(x) for x in info[4:]]
                comps_reverse.update({x:instance for x in comps[instance]})

            elif entry_type == "#cc":
                # AKS: Convert id to int
                instance = int(info[1])
                boxes[instance] = np.array(info[2:]).astype(int)

            elif entry_type == "#contours":
                # AKS: Convert id to int
                instance = int(info[1])

                points = info[2:]
                # points = [list(map(int, points[i:i+2])) for i in range(0, len(points), 2)]
                
                points = [[list(map(int, points[i:i+2]))] for i in range(0, len(points), 2)]
                if instance in contours:
                    contours[instance].append(points)
                else:
                    contours[instance] = [points]
            #Bryan added
            elif get_relations and entry_type == "r":
                #parentId, childId, class
                relations.append((info[1], info[2], info[3]))

        if mode == "contours":
            instances = sort_contours(contours)
        # CC mode
        else:
            instances = sort_CCs(boxes)

        sym_tags = []
        bboxes = []
        for cc in instances:
            key = comps_reverse.get(cc, None)
            if key is None:
                continue
            sym_tags.append(key)
            # print(" >> EntryType: " + entry_type )
            # print("    Line: " + str(info ))
            # Writes bounding box, value, entry_type
            # bbox = convBB( boxes[cc] )
            if mode == "contours":
                points = contours[cc]
                points_np = np.concatenate(points)
                bbox_min = points_np.min(axis=0)[0]
                bbox_max = points_np.max(axis=0)[0]
                # bbox_min = points_np.min(axis=0)
                # bbox_max = points_np.max(axis=0)
                bbox = [bbox_min[1], bbox_min[0], bbox_max[1],
                        bbox_max[0]]
                bboxes.append(bbox)
                out_list[-1][1][-1][-1][1].append(
                    (bbox[1], bbox[0], bbox[3], bbox[2],
                     symLabels[key], "c", cc, key, points))
            else:
                bbox = boxes[cc]
                bboxes.append(bbox)
                # print(info[2], bbox)
                out_list[-1][1][-1][-1][1].append(
                    (bbox[1], bbox[0], bbox[3], bbox[2],
                     symLabels[key], "c", cc, key))
        if bboxes:
            UL_y, UL_x = np.array(bboxes).min(axis=0)[:2].tolist()
            LR_y, LR_x = np.array(bboxes).max(axis=0)[2:].tolist()
            out_list[-1][DOCUMENT.PROTABLE.value][-1][0][REGION.BBOX.value] = (UL_x, UL_y, LR_x, LR_y)
        # Store if segmentation required or not
        out_list[-1][DOCUMENT.PROTABLE.value][-1][0][-2] = len(sym_tags) != len(set(sym_tags))
    if get_relations:
        return out_list, relations
    else:
        return out_list



def inkml2pro(tsv_dir, tsv_files, task="train", excludeFiles=None):
    # AKS: Use glob to avoid hidden files
    if task == "train":
        traces_only = False
    else:
        traces_only = True

    if excludeFiles:
        tsv_files = list(filter(lambda x: x not in excludeFiles, tsv_files))
    # Create map from filenames to PRO table for each
    # document.
    pro_table_list = []
    region_triples = []

    # Create map from filenames to PRO table for each
    # document.
    pro_table_list = []
    region_triples = []

    # Forcing all formula files / pages in 1 document using []
    for t in range(len([tsv_files])):
        # Read in PRO table
        pro_table_list.append(read_pro_inkml(tsv_dir, tsv_files,
                                             traces_only=traces_only)[0][1])

        # Create indexing triples (doc, page, region)
        pages = len(pro_table_list[-1])
        for p in range(pages):
            # Add tuples to list.
            region_count = len(pro_table_list[-1][p])
            t_list = [t] * region_count
            plist = [p] * region_count
            tuples = list(zip(t_list, plist,
                              list(range(region_count))))

            region_triples.extend(tuples)

    lg = True

    return tsv_files, pro_table_list, region_triples, None, lg


def parse_trace(trace):
    pairs = trace.text.strip().split(',')
    data = [[float(d) for d in pair.strip().split()][:2] for pair in pairs]
    return data

def parse_trace_group(trace_group):
    data = {"symbol": None, "traces": [],"symTag":None}
    for item in trace_group:
        if item.tag.endswith("annotation"):
            label=item.text
            label=label.replace('<','\\lt') 
            label=label.replace('>','\\gt')  
            data["symbol"] = label 
        if item.tag.endswith("annotationXML"):
            data["symTag"] = item.attrib["href"].replace(',','COMMA')
        if item.tag.endswith("traceView"):
            data["traces"].append(int(item.attrib["traceDataRef"]))
            data["traces"]=sorted(data["traces"], key = lambda x: x) #MM: soring stroke index in groups
    if data["symTag"] :return (data["symTag"], data["symbol"], data["traces"]) 

def read_pro_inkml(file_dir, file_paths, traces_only=False):
    """
        Read in list of LG files and return a list of (filename, PRO Table) pairs.
    """
    out_list = [("CROHME", [])]
    # AKS: Read from LG File
    # Append ( filename, empty-PRO )
    files = set()
    for filePath in file_paths:
        # Add page to current PRO table
        out_list[-1][1].append([])
        # Add region to final page in current PRO table
        out_list[-1][1][-1].append([(0, 0, 0, 0), [], 'FR'])

        (symLabels, comps, boxes) = {}, {}, {}
        # with open( path.join(fileDir, filePath), "r") as tsvFile:

        root = ET.parse(path.join(file_dir, filePath)).getroot()
        traces = {}
        labels = []
        count = 0
        for item in root:
            if item.tag.endswith('trace'):
                item_id = item.attrib['id']
                if bool(re.match(r'^-?\d+$', item_id)):
                    trace_id = int(item_id)
                else:
                    trace_id = count
                    count += 1
                #     print(filePath)
                #     print(item_id)
                #     files.add(filePath)
                # match = re.search(r'\d+', item_id)
                # trace_id = int(match.group())
                trace_data = parse_trace(item)
                traces[trace_id] = trace_data

            if item.tag.endswith('traceGroup'):
                for sub_item in item:
                    if sub_item.tag.endswith('traceGroup'):
                        if parse_trace_group(sub_item):
                            labels.append(parse_trace_group(sub_item))
                        # test stk level
        bboxes = []
        if traces_only:
            traces = [(int(p[0]), p[1]) for p in traces.items()]
            traces = sorted(traces)  # TODO: sort instances based on minx in trace points
            for idx, trace in traces:
                ul_x, ul_y, lr_x, lr_y = np.array(trace).min(axis=0).tolist() + \
                            np.array(trace).max(axis=0).tolist()
                bboxes.append([ul_x, ul_y, lr_x, lr_y])
                out_list[-1][1][-1][-1][1].append(
                (ul_x, ul_y, lr_x, lr_y, "_", "c", idx, None, trace) )


        else:
            # (MM) sort labels based on stk GROUPS
            labels.sort(key=lambda x: int(x[2][0]))
            for symTag, symbol, indices in labels:
                for index in indices:
                    ul_x, ul_y, lr_x, lr_y = np.array(traces[index]).min(axis=0).tolist() + \
                                                np.array(traces[index]).max(axis=0).tolist()
                    bboxes.append([ul_x, ul_y, lr_x, lr_y])
                    out_list[-1][1][-1][-1][1].append( \
                        ( ul_x, ul_y, lr_x, lr_y, symbol, "c", index, symTag, traces[index] ) )

        UL_x, UL_y = np.array(bboxes).min(axis=0)[:2].tolist()
        LR_x, LR_y = np.array(bboxes).max(axis=0)[2:].tolist()
        out_list[-1][1][-1][0][0] = (UL_x, UL_y, LR_x, LR_y)
    return out_list


################################################################
# Filters
################################################################

##################################
# Bounding boxes
##################################

def min_bb_thickness(t_width, t_height):
    # RZ: Enforce minimum width and height

    def t_filter(bb):
        (xMin, yMin, xMax, yMax) = bb
        bb_width = xMax - xMin + 1
        bb_height = yMax - yMin + 1

        pass_filter = (bb_width >= t_width and bb_height >= t_height)
        return pass_filter

    return t_filter


def filter_bb_list(test_fn, bb_list):
    # Create one blank list per page.
    out_list = [[]] * len(bb_list)

    for i in range(len(bb_list)):
        out_list[i] = list(filter(test_fn, bb_list[i]))

    return out_list


##################################
# Regions in PRO tables
##################################
def min_max_object_test(min_count, max_count, type_list):
    # Require having at least the minCount for all types shown
    def land(a, b):
        return a and b

    def r_filter(region):
        type_count = {typeName: 0 for typeName in type_list}
        for r in region[1]:
            oType = r[5]
            if oType in type_list:
                type_count[oType] += 1
        threshold_tests = [min_count <= x <= max_count
                           for x in list(type_count.values())]

        return reduce(land, threshold_tests)

    return r_filter


def max_height_test(pixel_height):
    def height_filter(region):
        height = region[0][3] - region[0][1]
        return height <= pixel_height

    return height_filter

def max_width_test(pixel_width):
    def width_filter(region):
        width = region[0][2] - region[0][0]
        return width <= pixel_width

    return width_filter

# RZ: Heuristic test to try and capture words comprised only of letters,
# possibly contained by parens or followed by a punctuation mark.
def alphaWord(word):
    if word.isalpha() or \
            word[0] == '(' and word[-1] == ')' and word[1:-1].isalpha() or \
            word[-1] in ['.', ',', ':', ';'] and word[0:-1].isalpha():
        return True
    else:
        return False


# Remove *single* words in standard dictionaries, emails
def in_dictionary(dict_name_list, remove_alpha=False, report=True):
    def dictFilter(region):
        words = 0
        object_list = region[1]
        keep_region = True
        removing = None

        for obj in object_list:
            orig_word = obj[OBJECT.LABEL.value]
            if obj[5] == 'W':
                words += 1
                if words > 1:
                    keep_region = True
                    break

                if '@' in orig_word:
                    # Check for email addresses via '@'
                    keep_region = False
                    removing = orig_word
                else:
                    # Check word with and without punctuation removed
                    word = obj[OBJECT.LABEL.value].translate(str.maketrans('', '', string.punctuation))

                    for name in dict_name_list:
                        d = enchant.Dict(name)
                        if word == '' or d.check(word) or d.check(orig_word) \
                                or (remove_alpha and alphaWord(orig_word)):
                            keep_region = False
                            removing = orig_word
                            break

        # Produce output for removed terms if asked.
        if report and not keep_region:
            logging.debug("  >>  Removing: " + removing + "  \t(in dictionary)")

        return keep_region

    return dictFilter


'''
    Take a page of regions, return interval tree to test intersection.
'''

def obj_int(region_list):
    t_y = IntervalTree()
    
    # Create interval tree for Y direction 
    # AKS: Debug: Include upper limit (+1 on max value)
    for r in range( len(region_list) ):
        for o in range(len(region_list[r][1])):
            (minX, minY, maxX, maxY) = region_list[r][1][o][:4]
            t_y[minY:maxY + 1] = Interval( minX, maxX + 1, o)

    # Return interval tree for all objects on the page.
    return t_y 

'''
    Take a list of objects, return interval tree to test intersection,
    and two dictionaries for containment, and membership.
'''
def obj_type_int(obj_list, contain_type, member_type):
    t_y = IntervalTree()

    # Dictionaries to later record intersections
    contain_dict = {}
    member_dict = {}
    for o in range( len(obj_list) ):
        (minX, minY, maxX, maxY, _, typeLabel) = obj_list[o]
        if typeLabel == contain_type:
            # AKS: Debug: Include upper limit
            t_y[minY:maxY + 1] = Interval( minX, maxX + 1, o )
            contain_dict[o] = []

        elif typeLabel == member_type:
            member_dict[o] = []

    # Return interval trees defining intersections with objects of given type
    # Plus dictionary with keys for later member analysis
    return t_y, contain_dict, member_dict



'''
    Prune objects that intersect objects from a separate list
    (e.g., to prune CCs intersecting PDF symbols) if the area of
    intersection is greater than a percentage of the object area.
'''


def obj_intersection_test(region_obj_table, threshold):
    # Create pairs of interval trees, one per page (X and Y)
    y_int_trees = [obj_int(region_obj_table[p])
                  for p in range(len(region_obj_table))]

    # Use currying to modify filter by page.
    def gen_o_filter(p):
        def o_filter(obj):
            # Find objects that intersect this object
            # AKS: Debug: Include upper limit
            (minX, minY, maxX, maxY) = obj[:4]
            tY = y_int_trees[p]
            y_matches = tY[minY:maxY + 1]

            # If we have matches, test intersection size.
            if len(y_matches) > 0:
                obj_area = (maxX - minX + 1) * (maxY - minY + 1)
                thr_area = obj_area * threshold

                for (beginY, endY, xInterval) in y_matches:
                    # RZ: endY is actually maxY + 1
                    y_length = min(endY - 1, maxY) - max(beginY, minY) + 1
                    x_length = xInterval.overlap_size(minX, maxX + 1)
                    int_area = x_length * y_length
                    
                    if int_area > thr_area:
                        return False

            # If no intersections or too little overlap.
            return True

        return o_filter

    return gen_o_filter


##################################
# Object filters
##################################
def obj_filter_values(value_list):
    value_set = set(value_list)

    def isNotValue(obj):
        # (_, _, _, _, oValue, _) = obj
        return not (obj[OBJECT.LABEL.value] in value_set)

    return isNotValue

def obj_filter_values_string(value_string):
    def isNotValue(obj):
        # (_, _, _, _, oValue, _) = obj
        return not (obj[OBJECT.LABEL.value] in value_string)

    return isNotValue


def obj_keep_types(type_list):
    type_set = set(type_list)

    def isType(obj):
        # (_, _, _, _, _, oType) = obj
        return obj[OBJECT.TYPE.value] in type_set

    return isType


def apply_obj_within_region(region_obj_table):
    def obj_within_region(obj, region_bb, region_area):
        # Object and region bounding boxes
        (obj_min_x, obj_min_y, obj_max_x, obj_max_y) = obj[:4]
        (reg_min_x, reg_min_y, reg_max_x, reg_max_y) = region_bb

        # Check if the object is within the region bounds on all four sides
        inside = (reg_min_x <= obj_min_x <= reg_max_x or
                reg_min_y <= obj_min_y <= reg_max_y or
                reg_min_x <= obj_max_x <= reg_max_x or
                reg_min_y <= obj_max_y <= reg_max_y)

        obj_area = (obj_max_x - obj_min_x + 1) * (obj_max_y - obj_min_y + 1)
        return inside and obj_area <= region_area

    filtered_region_obj_table = []

    for page in region_obj_table:
        filtered_page = []
        for region in page:
            region_bb = region[REGION.BBOX.value]
            [ rminX, rminY, rmaxX, rmaxY ] = region_bb
            r_width = rmaxX - rminX + 1
            r_height = rmaxY - rminY + 1
            r_area = r_width * r_height

            filtered_objs = [obj for obj in region[REGION.OBJECTS.value] 
                             if obj_within_region(obj, region_bb, r_area)]
            filtered_page.append((region_bb, filtered_objs, region[REGION.TYPE.value]))
        filtered_region_obj_table.append(filtered_page)

    return filtered_region_obj_table


##################################
# Combining tests
##################################
def combine_and_tests(test_list):
    def land(a, b):
        return a and b

    # Compose tests in one function (avoid multiple passes over input)
    # If any test returns False, the output for the region is False.
    def r_filter(region):
        bool_values = [fn(region) for fn in test_list]
        return reduce(land, bool_values)

    return r_filter


'''
    Filter regions and objects in regions. Region and object filters
    should take a region entry ( tuple with element [1] containing obj. list).
'''


def filter_pr_table(region_filter_fn, object_filter_fn, region_obj_table):
    # Note: this applies local filters (i.e., removing items based on an isolated
    # region or object). Returns a modified table with references to the original t
    # table (reusing references to original elements).

    output_obj_table = [[] for _ in range(len(region_obj_table))]
    for p in range(len(region_obj_table)):
        # Apply region filter (per page)
        if region_filter_fn is not None:
            output_obj_table[p] = list(filter(region_filter_fn, region_obj_table[p]))
        else:
            output_obj_table[p] = region_obj_table[p]

        # On remaining regions, apply object filter (looks at independent objs)
        if object_filter_fn is not None:
            for r in range(len(output_obj_table[p])):
                output_obj_table[p][r] = (output_obj_table[p][r][REGION.BBOX.value],
                                          list(filter(object_filter_fn,
                                                      output_obj_table[p][r][REGION.OBJECTS.value])),
                                          output_obj_table[p][r][REGION.TYPE.value])

    return output_obj_table


################################################################
# Transformations
################################################################

def grow_for_contents(page_sizes): #(png_width, png_height):
    def grow_region( region_page_pair ):
        # RZ: Modification to account for different page sizes
        ( region, page ) = region_page_pair

        (min_x, min_y, max_x, max_y) = region[REGION.BBOX.value]
        objs = region[REGION.OBJECTS.value]

        ( png_width, png_height ) = page_sizes[page]

        # RZ: Update top-left, bottom-right to contain all objects.
        # DEBUG: Insure that coordinates remain inside the image.
        #  Some objects seem to exist 'outside' the visible page
        #  (e.g., negative y-coordinates).
        for obj in objs:
            (o_min_x, o_min_y, o_max_x, o_max_y) = obj[:4]
            min_x = max(0, min(min_x, o_min_x))
            min_y = max(0, min(min_y, o_min_y))
            max_x = min(png_width - 1, max(max_x, o_max_x))
            max_y = min(png_height - 1, max(max_y, o_max_y))
        # p_check( str( (min_x, min_y, max_x, max_y) ), "Updated")

        # Return updated bounding box, same objects and type.
        return (min_x, min_y, max_x, max_y), objs, region[REGION.TYPE.value]

    return grow_region


def crop_contents( page_sizes ):
    def crop_region( region_page_pair ):
        ( region, page_idx ) = region_page_pair 
        ( png_width, png_height ) = page_sizes[ page_idx ]

        # Use BB of first object to initialize; ignore given
        # region BB to permit shrinkage.
        (min_x, min_y, max_x, max_y) = region[0]
        objs = region[1]

        # Create a tightly cropped BB (all corners)
        for obj in objs[1:]:
            (o_min_x, o_min_y, o_max_x, o_max_y) = obj[:4]
            min_x = max(0, min(min_x, o_min_x))
            min_y = max(0, min(min_y, o_min_y))
            max_x = min(png_width - 1, max(max_x, o_max_x))
            max_y = min(png_height - 1, max(max_y, o_max_y))

        # Return updated bounding box, same objects and type.
        return (min_x, min_y, max_x, max_y), region[1], region[2]

    return crop_region




'''
    Simple heuristics to remove symbols not covering a CC, and modify
    symbol BBs and remove CCs for well-aligned boxes. Poor alignment out
    of PDF data (e.g., from OCR misalignment) may have poor results.

    Threhold is a percentage.
'''


def rectify_container_members(container_type='S', member_type='c',
                              threshold=INTERSECTION_THRESHOLD, crop=True):
    def rectify( region_page_pair ):
        (region, _) = region_page_pair
        pruned_containers = set()
        ccs_in_pruned = set()
        new_obj_list = []

        # Construct intersection tree (Y-direction)
        obj_list = region[1]
        (tY, containerDict, memberDict) = \
                obj_type_int(obj_list, container_type, member_type)

        # Find intersections, avoid members spanning multiple containers
        for o in range(len(obj_list)):
            (minX, minY, maxX, maxY, _, objType) = obj_list[o]

            if objType == member_type:
                # Set intersection threshold, including case for narrow symbols
                obj_area = (maxX - minX + 1) * (maxY - minY + 1) 
                thr_area = obj_area * threshold
                if (maxX - minX) <= 2 or (maxY - minY) <= 2:
                    thr_area = obj_area * INTERSECTION_THRESHOLD_THIN

                # AKS: Debug: Include upper limit
                y_intervals = tY[minY:maxY + 1]
                for (beginY, endY, xInterval) in y_intervals:
                    # RZ: endY is actually maxY + 1
                    y_length = min(endY - 1, maxY) - max(beginY, minY) + 1
                    x_length = xInterval.overlap_size(minX, maxX + 1)
                    #x_length = min(xInterval.end, maxX) - max(xInterval.begin, minX) + 1
                    int_area = x_length * y_length

                    if int_area >= thr_area:
                        cId = xInterval.data
                        memberDict[o].append(cId)
                        containerDict[cId].append(o)

                        # Prune container with members in another container.
                        if len(memberDict[o]) > 1:
                            pruned_containers = \
                                pruned_containers.union(memberDict[o])
                            for contId in memberDict[o]:
                                ccs_in_pruned = \
                                    ccs_in_pruned.union(containerDict[contId])

        # Construct new output object list
        for cId in containerDict.keys():
            if len(containerDict[cId]) < 1 or cId in pruned_containers:
                continue

            # Access region data
            (origMinX, origMinY, origMaxX, origMaxY, oLabel, oType) = \
                obj_list[cId]

            # Crop region if asked.
            if crop:
                (c_min_x, c_min_y, c_max_x, c_max_y) = (9999, 9999, -1, -1)
                for objId in containerDict[cId]:
                    (minX, minY, maxX, maxY) = obj_list[objId][:4]
                    c_min_x = min(c_min_x, minX)
                    c_min_y = min(c_min_y, minY)
                    c_max_x = max(c_max_x, maxX)
                    c_max_y = max(c_max_y, maxY)

                new_obj_list.append(
                    (c_min_x, c_min_y, c_max_x, c_max_y, oLabel, oType))
            else:
                # Otherwise, use defined boundinx box.
                new_obj_list.append(
                    (origMinX, origMinY, origMaxX, origMaxY, oLabel, oType))

        # Objects not in a container.
        for o in memberDict.keys():
            if len(memberDict[o]) != 1 or o in ccs_in_pruned:
                new_obj_list.append(obj_list[o])

        # Return same BB and type, and new object list
        return region[0], new_obj_list, region[2]

    return rectify


'''
    Transform regions and/or objects in a pro table.
'''

def add_page_to_elements( region_list, page_idx ):
    return [ (region, page_idx) for region in region_list ]

def transform_pr_table(region_transform_fn, object_transform_fn, region_obj_table):
    output_obj_table = [[] for _ in range(len(region_obj_table))]
    for p in range(len(region_obj_table)):
        # Apply region filter.
        if region_transform_fn is not None:
            output_obj_table[p] = list(map(region_transform_fn, \
                    add_page_to_elements( region_obj_table[p], p ) ))
        else:
            output_obj_table[p] = region_obj_table[p]

        for r in range(len(output_obj_table[p])):
            # Apply obj transform in remaining regions.
            if object_transform_fn is not None:
                output_obj_table[p][r][REGION.OBJECTS.value] = \
                    list(map(object_transform_fn, output_obj_table[p][r][REGION.OBJECTS.value]))

    return output_obj_table


################################################################
# Selection in page-region-object (pro) tables
################################################################
def region_bb_list(pro_table):
    output_list = [[] for _ in range(len(pro_table))]

    for p in range(len(pro_table)):
        for r in range(len(pro_table[p])):
            bbox = pro_table[p][r][REGION.BBOX.value]
            output_list[p].append(bbox)

    return output_list


################################################################
# Region intersection and Merging
################################################################
def merge_objects(oa_id_x, oa_list, ob_id_x, ob_list):
    # print( str(oaIdx) + " " + str(obIdx) )
    # For simplicity, repetitions of identical objects remain
    # repeated in the merged list.
    a_tuple = oa_list[oa_id_x]
    b_tuple = ob_list[ob_id_x]

    (a_min_x, a_min_y, a_max_x, a_max_y) = a_tuple[:4]
    (b_min_x, b_min_y, b_max_x, b_max_y) = b_tuple[:4]
    a_bb = (a_min_x, a_min_y, a_max_x, a_max_y)
    b_bb = (b_min_x, b_min_y, b_max_x, b_max_y)

    if a_bb == b_bb or a_min_x < b_min_x or (a_min_x == b_min_x and a_min_y <= b_min_y):
        # print("A")
        # If BBs identical, or a top-left corner is to left / above b_tuple
        return a_tuple, oa_id_x + 1, ob_id_x
    else:
        # print("B")
        # Select from list B
        return b_tuple, oa_id_x, ob_id_x + 1


def merge_pro_tables(pro_table_a, pro_table_b):
    # ASSUMPTION: both page-region-object (pro) tables contain the same number
    # of pages.  NOTE: identical regions and objects are merged; if regions or
    # objects have different labels, they remain separate entries.
    pro_output_table = [[] for _ in range(len(pro_table_a))]

    # Sort both tables, and round coordinates.
    table_a = round_pro_coords(sort_page_regions(pro_table_a))
    table_b = round_pro_coords(sort_page_regions(pro_table_b))

    # show_pro_table( table_a, "TABLE A")

    for p in range(len(table_a)):
        # print("PAGE: " + str(p) )
        pro_output_table[p] = []
        a_idx = b_idx = 0

        # Compare regions, merge objects in identical regions
        while a_idx < len(table_a[p]) and b_idx < len(table_b[p]):
            # print( "a_idx:" + str(a_idx) + " b_idx: " + str(b_idx) )
            a_bb = table_a[p][a_idx][REGION.BBOX.value]
            (a_min_x, a_min_y, a_max_x, a_max_y) = a_bb
            a_objects = table_a[p][a_idx][REGION.OBJECTS.value]
            a_rtype = table_a[p][a_idx][REGION.TYPE.value]

            b_bb = table_b[p][b_idx][REGION.BBOX.value]
            (b_min_x, b_min_y, b_max_x, b_max_y) = b_bb
            b_objects = table_b[p][b_idx][REGION.OBJECTS.value]
            b_rtype = table_b[p][b_idx][REGION.TYPE.value]

            # print("REGION BOXES:")
            # print(a_bb)
            # print(b_bb)

            # Record whether bounding boxes are identical
            same_box = (a_bb == b_bb)

            if same_box:
                if a_rtype == b_rtype:
                    # Merge objects in identical regions (same BBs AND types)
                    # ASSUMPTION: neither list has multiple copies of a region
                    # with the same BB and type.
                    oa_idx = ob_idx = 0
                    oa_list = table_a[p][a_idx][REGION.OBJECTS.value]
                    ob_list = table_b[p][b_idx][REGION.OBJECTS.value]
                    merged_objs = []

                    while oa_idx < len(table_a[p][a_idx][REGION.OBJECTS.value]) and \
                            ob_idx < len(table_b[p][b_idx][REGION.OBJECTS.value]):
                        # print("  A Obj List: " +  str(oa_list[ : ]) + "\n" + \
                        #        "  B Obj List: " + str(ob_list[ : ])  )

                        (nextObj, oa_idx, ob_idx) = \
                            merge_objects(oa_idx, oa_list, ob_idx, ob_list)

                        merged_objs.append(nextObj)
                        # print("BRANCH ----L-------")
                        # print(" Appended: " + str( nextObj ))

                    if oa_idx < len(oa_list):
                        merged_objs.extend(oa_list[oa_idx:])
                        # print("BRANCH ---K----")
                        # print(" Appended: " + str( oa_list[oa_idx:] ))

                    elif ob_idx < len(ob_list):
                        merged_objs.extend(ob_list[ob_idx:])
                        # print("BRANCH ---M----")
                        # print(" Appended: " + str( ob_list[ob_idx:] ))

                    pro_output_table[p].append((a_bb, merged_objs, a_rtype))

                    # RZ: Debug, need to advance both pointers!
                    a_idx += 1
                    b_idx += 1
                    # print("BRANCH A1")

                else:
                    # Keep non-identical regions *separate*
                    # DEBUG: Avoid mishandling multiple copies by
                    # advancing in only one list.
                    pro_output_table[p].append((a_bb, a_objects, a_rtype))
                    a_idx += 1

                    # print("BRANCH A :: 2")
                    # print(" Appended: " + str( oa_list[a_idx - 1] ))

            elif a_min_x < b_min_x or (not same_box and
                                       a_min_x == b_min_x and a_min_y <= b_min_y):
                # Regions not identical -- Add next region from table_a
                pro_output_table[p].append((a_bb, a_objects, a_rtype))
                a_idx += 1

                # print("BRANCH B")
                # print(" Appended: " + str( (a_bb, a_objects, a_rtype) ))

            else:
                # Add next region from table_b
                pro_output_table[p].append((b_bb, b_objects, b_rtype))
                b_idx += 1

                # print("BRANCH C")
                # print(" Appended: " + str( (b_bb, b_objects, b_rtype) ))

            # show_pro_table( pro_output_table, "TABLE at a_idx: " + str(a_idx) + \
            #        " b_idx: " + str(b_idx) )

        # Add remaining region entries to the output page entry.
        if a_idx < len(table_a):
            pro_output_table[p].extend(table_a[p][a_idx:])
            # print("BRANCH X")
            # print(" Appended: " + str( table_a[p][ a_idx: ]))

        elif b_idx < len(table_b):
            pro_output_table[p].extend(table_b[p][b_idx:])
            # print("BRANCH Y")
            # print(" Appended: " + str( table_b[p][ b_idx: ]))

    # show_pro_table( pro_output_table, "TABLE at a_idx: " + str(a_idx) + \
    #   " b_idx: " + str(b_idx) )

    return pro_output_table


def merge_pro_tables_torchrun(pro_table_a, pro_table_b):
    # ASSUMPTION: both page-region-object (pro) tables contain the same number
    # of pages.  NOTE: identical regions and objects are merged; if regions or
    # objects have different labels, they remain separate entries.
    pro_output_table = [[] for _ in range((len(pro_table_a) + len(pro_table_b)))]

    # Sort both tables, and round coordinates.
    table_a = round_pro_coords(sort_page_regions(pro_table_a))
    table_b = round_pro_coords(sort_page_regions(pro_table_b))

    for p in range(len(table_a)):
        pro_output_table[p].extend(table_a[p])


    for p in range(len(table_b)):
        pro_output_table[len(table_a) + p].extend(table_b[p])

    return pro_output_table

'''
    In-place sort for objects within regions, and regions on each page
    by minX then minY.
    RZ Mod: sort page regions top-down, left-right. Sort formulas
      left-right, top-down, to roughly match scanning/reading conventions.
'''
def sort_page_regions(region_obj_table, sort_objects=True,
                      sort_obj_types=True, sort_obj_order="xy",
                      sort_region_order="yx"):
    # Object sort order
    sort_obj_order = sort_order_map[sort_obj_order]
    # Region Sort order - default: yx: top-down left-right
    key1 = lambda region: region[0][0]
    key2 = lambda region: region[0][1]
    if sort_region_order == "xy":
        key1, key2 = key2, key1
    # If normal mode 
    for p in range(len(region_obj_table)):
        # Sort objects in regions by x, then y, then object type
        for r in range(len(region_obj_table[p])):
            objects = sorted(region_obj_table[p][r][REGION.OBJECTS.value], key=sort_obj_order)
            # If sort by obj type
            if sort_obj_types:
                objects = sorted(objects, key=itemgetter(5), reverse=True)
            region_obj_table[p][r] = (region_obj_table[p][r][REGION.BBOX.value], objects,
                                     region_obj_table[p][r][REGION.TYPE.value])

        # Sort regions (2-pass: by minX, minY -- sorts stable )
        # Make sort for options done by default, permit this not being
        # done to support fixing output ordering in files.
        if sort_objects:
            region_obj_table[p] = sorted(sorted(region_obj_table[p], 
                                                key=key1), key=key2)

    return region_obj_table

""" Find characters intersecting regions at page level and annotate the
    characters to the regions.
    Produce a list of regions, in each region a list of char BBs that intersect the
    passed region BBs.
"""
def annotate_chars(params):
    region_bbs, char_bbs, rtype = params
    num_regions_on_page = len(region_bbs)
    graphics_region_bbs = [[] for _ in range(num_regions_on_page)]
    # region_idx = rtree.index.Index()
    t_y = IntervalTree()

    # dTimer.check("Initial:")
    for r in range(len(region_bbs)):
        # Define region entry as region bounding box, and empty char. list
        (minX, minY, maxX, maxY) = region_bbs[r]
        t_y[minY: maxY + 1] = Interval(minX, maxX + 1, r)
        # AKS: Add placeholder region entries
        # dTimer.check("Initial:")
        # region_idx.insert(r, (minX, minY, maxX, maxY))
        # dTimer.check("Insertion:")
        graphics_region_bbs[r] = ([minX, minY, maxX, maxY], [], rtype)

    # dTimer.qcheck("Region-tree: building trees completed")
    region_matches = set()
    for c in range(len(char_bbs)):
        char_bb = char_bbs[c]
        # AKS: Fix previous bug
        (minX, minY, maxX, maxY) = char_bb[:4]

        # AKS: Get regions intersecting with the characters
        # dTimer.check("Initial:")
        # region_intersection = set(region_idx.intersection((minX, minY,
                                                           # maxX, maxY)))
        # dTimer.check("Intersection:")
        # region_matches = region_matches.union(region_intersection)

        member_regions = set()
        y_matches = t_y[minY : maxY + 1]

        for (_, _, xInterval) in y_matches:
            if xInterval.overlap_size(minX, maxX + 1) > 0:
                member_regions.add( xInterval.data )

        region_matches = region_matches.union(member_regions)

        # Add character box to output structure.
        for m in member_regions:
        # for m in region_intersection:
            graphics_region_bbs[m][1].append(char_bb)

    return [graphics_region_bbs]

'''
    Produce a list of pages, each containing a list of regions,
    in each region a list of BBs that intersect the passed region
    BBs.
'''
def region_intersection(page_region_bbs, rtype, page_char_bbs, 
                        metrics=None, filename=None, sort_obj_order="xy",
                        sort_region_order="yx", sort_obj_types=True):
    # Assumption: two inputs are for the same document.
    # Assumption: these are standard image BB coordinates (top-left, bottom-right)
    # Note: using 0-based indexing in this function (for list ops)

    # Set up nested list/tuple structure for the page.
    num_pages = len(page_region_bbs)

    detection_count =len( sum( page_region_bbs, []) )
    # check('* YOLO detections', detection_count )
    # print('')

    # RZ : Addition
    if metrics:
        metrics[ filename ][ 'YOLO-detections' ] = detection_count

    # dTimer.check("Initial")
    
    page_graphics_region_bbs = []
    for p in range(num_pages):
        page_graphics_region_bbs.append(annotate_chars((page_region_bbs[p],
                                                        page_char_bbs[p], rtype))[0])
    # dTimer.check("Series")

    # # TODO: The parallel region intersection is slower for some reason
    # input_list = list(zip(page_region_bbs, page_char_bbs, repeat(rtype)))
    # page_graphics_region_bbs = map_concat(annotate_chars, input_list) 
    # dTimer.check("Parallel")
    
    # RZ Addition
    # Organization of page_graphics_region_bbs is a list of pages, each containing a list of region bounding boxes
    p = 0
    non_empty_regions = 0
    graphics_in_regions = 0
    for page in page_graphics_region_bbs:
        for fr in page:
            #pcheck('fr[0]',fr[0])
            #pcheck('len(fr[1])',len(fr[1]))
            if len(fr[REGION.OBJECTS.value]) < 1:
                # check('>> Empty region (page_id,bb)', (p, fr[REGION.BBOX.value]))
                pass
            else:
                non_empty_regions += 1
                graphics_in_regions += len(fr[REGION.OBJECTS.value])
                #check('* Region graphics (page_id,#)',(p, len(fr[1])))
        p += 1

    if metrics:
        metrics[ filename ][ 'Regions-with-PDF-graphics' ] = non_empty_regions
        metrics[ filename ][ 'Graphics-objects-in-regions' ] = graphics_in_regions

    # For readability, return regions and objects sorted by minX / minY
    return sort_page_regions(page_graphics_region_bbs, sort_obj_order=sort_obj_order,
                             sort_region_order=sort_region_order,
                             sort_obj_types=sort_obj_types), metrics

def obj_occupies_region(region, ratio_threshold=0.8):
    # Region area
    [ rminX, rminY, rmaxX, rmaxY ] = region[0]
    r_width = rmaxX - rminX + 1
    r_height = rmaxY - rminY + 1
    r_area = r_width * r_height

    object_area = 0

    # Area occupied by the objects
    objects = region[1]
    if len(objects):
        object_bboxes = np.array(list(map(itemgetter(0,1,2,3), objects)))
        object_bboxes_min = object_bboxes[:, :2].min(axis=0)
        object_bboxes_max = object_bboxes[:, 2:].max(axis=0)
        object_h, object_w = object_bboxes_max - object_bboxes_min + 1
        object_area = object_h * object_w

    # Ratio of area
    area_ratio = object_area / r_area
    return area_ratio >= ratio_threshold


def has_graphical_char(objects):
    obj_labels = set(map(itemgetter(OBJECT.LABEL.value), objects))
    obj_labels = list(map(lambda x:set(x.split(" ")), obj_labels))
    obj_with_curves = list(filter(lambda x: "curve" in x and len(x) > 1, obj_labels))
    return len(obj_with_curves) > 0

def has_char(objects):
    obj_types = set(map(itemgetter(OBJECT.TYPE.value), objects))
    return "S" in obj_types

def has_only_char(objects):
    obj_types = set(map(itemgetter(OBJECT.TYPE.value), objects))
    return obj_types == {"S"}

def separate_visual_regions(region_obj_table, page_imgs, metrics=None,
                            filename=None, visualize_formulas=False,
                            form_img_dir=None):
    if visualize_formulas:
        viz_dir = os.path.join(form_img_dir, filename)
        os.makedirs(viz_dir, exist_ok=True)
    visual_imgs = []
    mapping = {}
    digital_regions = 0
    visual_regions = 0
    graphics_in_regions = 0
    digital_table = [[] for _ in range(len(region_obj_table))]
    empty_fr = [None, [], 'FR']

    for idx_page, page in enumerate(region_obj_table):
        mapping[idx_page] = {}
        p_img = page_imgs[idx_page]
        for idx_fr, fr in enumerate(page):
            #pcheck('fr[0]',fr[0])
            #pcheck('len(fr[1])',len(fr[1]))
            region_bbox = fr[REGION.BBOX.value]
            objects = fr[REGION.OBJECTS.value]

            if not len(objects):
                region_type = "visual"
            else:
                # [IF there are only actual characters in the region OR
                    # [If the objects occupy 80% of the region AND 
                           # [Region has atleast one character OR 
                           #  Region has no graphical character]
                   # ]
               # ]
                if (has_only_char(objects) or (obj_occupies_region(fr, ratio_threshold=OBJ_REGION_THRESHOLD) and
                    (has_char(objects) or not has_graphical_char(objects)))):
                    region_type = "digital"
                else:
                    region_type = "visual"
            if region_type == "visual":
                visual_regions += 1
                mapping[idx_page][idx_fr] = region_type
                
                rminX, rminY, rmaxX, rmaxY = [round(c) for c in region_bbox]
                clip_img = p_img[ rminY:rmaxY+1, rminX:rmaxX+1 ]

                reg_dict = {'page': idx_page, 'fr': idx_fr, 'img': clip_img,
                            'id': str(idx_page)+'_'+str(idx_fr), 'bbox': region_bbox}
                visual_imgs.append(reg_dict)
                if visualize_formulas:
                    cv2.imwrite(osp.join(viz_dir, reg_dict['id'] + '.png'), clip_img)
                digital_table[idx_page].append(empty_fr)
            else:
                digital_regions += 1
                graphics_in_regions += len(objects)
                mapping[idx_page][idx_fr] = region_type
                digital_table[idx_page].append(fr)
                #check('* Region graphics (page_id,#)',(p, len(fr[1])))

    if metrics:
        metrics[ filename ][ 'Regions-with-PDF-graphics' ] = digital_regions
        metrics[ filename ][ 'Regions-without-PDF-graphics' ] = visual_regions
        metrics[ filename ][ 'Graphics-objects-in-regions' ] = graphics_in_regions

    return visual_imgs, metrics, mapping, digital_table

# This code by Abhisek is not used anymore
def region_intersection_with_visual(page_region_bbs, rtype, page_char_bbs, pdf_img_dir,
                        metrics=None, filename=None, sort_obj_order="xy",
                        sort_region_order="yx", sort_obj_types=True, vis_pad=0.1):
    # Assumption: two inputs are for the same document.
    # Assumption: these are standard image BB coordinates (top-left, bottom-right)
    # Note: using 0-based indexing in this function (for list ops)

    # Set up nested list/tuple structure for the page.
    num_pages = len(page_region_bbs)

    detection_count =len( sum( page_region_bbs, []) )
    # check('* YOLO detections', detection_count )
    # print('')

    # RZ : Addition
    if metrics:
        metrics[ filename ][ 'YOLO-detections' ] = detection_count

    # dTimer.check("Initial")
    
    page_graphics_region_bbs = []
    for p in range(num_pages):
        page_graphics_region_bbs.append(annotate_chars((page_region_bbs[p],
                                                        page_char_bbs[p], rtype))[0])
    # dTimer.check("Series")

    # # TODO: The parallel region intersection is slower for some reason
    # input_list = list(zip(page_region_bbs, page_char_bbs, repeat(rtype)))
    # page_graphics_region_bbs = map_concat(annotate_chars, input_list) 
    # dTimer.check("Parallel")
    
    # RZ Addition
    # AD Mod: For ChemScraperV2 (Digital + Visual) store the Digital And Visual Separately
    # Organization of page_graphics_region_bbs is a list of pages, each containing a list of region bounding boxes
    digital_table = [[] for _ in range(len(page_graphics_region_bbs))]
    visual_table = [[] for _ in range(len(page_graphics_region_bbs))]
    visual_imgs = []
    digital_regions = 0
    visual_regions = 0
    graphics_in_regions = 0

    page_graphics_region_bbs = sort_page_regions(page_graphics_region_bbs, sort_obj_order=sort_obj_order,
                             sort_region_order=sort_region_order,
                             sort_obj_types=sort_obj_types)
    mapping = {}
    for idx_page, page in enumerate(page_graphics_region_bbs):
        mapping[idx_page] = {}
        for idx_fr, fr in enumerate(page):
            #pcheck('fr[0]',fr[0])
            #pcheck('len(fr[1])',len(fr[1]))
            # import pdb;pdb.set_trace()
            if len(fr[1]) < 3:
                # check('>> Found Possible Visual region (page_id,bb)', (idx_page,fr[0]))
                visual_regions += 1
                visual_table[idx_page].append(fr)
                mapping[idx_page][idx_fr] = "visual"
            else:
                digital_regions += 1
                graphics_in_regions += len(fr[1])
                digital_table.append(fr)
                mapping[idx_page][idx_fr] = "digital"
                #check('* Region graphics (page_id,#)',(p, len(fr[1])))
            
    # Now send the Visual Table to Grow CCs
    visual_contourgrown_table, region_imgs = get_cc_objects(pdf_img_dir, visual_table, 'FR', image=True)
    # Pack the visual Imgs
    for idx_page, p in enumerate(visual_contourgrown_table):
        for idx_fr, r in enumerate(p):
            reg_dict = {'page': idx_page, 'fr': idx_fr, 'img': region_imgs[idx_page][idx_fr], 
                        'id': str(idx_page)+'_'+str(idx_fr), 'bbox': r[0]}
            visual_imgs.append(reg_dict)

    if metrics:
        metrics[ filename ][ 'Regions-with-PDF-graphics' ] = digital_regions
        metrics[ filename ][ 'Regions-without-PDF-graphics' ] = visual_regions
        metrics[ filename ][ 'Graphics-objects-in-regions' ] = graphics_in_regions

    # For readability, return regions and objects sorted by minX / minY
        
    return page_graphics_region_bbs, visual_imgs, metrics, mapping

################################################################
# Parser output visualization functions
################################################################
# RZ: Currently unused
def create_dot_pdf(output_dir, model_name, last_epoch):
    # RZ: Separating visualization from parsing function
    start = time.time()

    lg_out_dir = osp.join(output_dir)
    os.makedirs(lg_out_dir, exist_ok=True)
    dot_out_dir = output_dir
    os.makedirs(dot_out_dir, exist_ok=True)
    results_dir = osp.join(lg_out_dir, model_name, "outputs_" + model_name +
                           "_" + last_epoch)

    logging.debug("\n[ Creating dot/PDF files for Formulas ]")
    logging.debug("  Output formula dot/PDF dir: {}".format(os.path.abspath(dot_out_dir)))

    # Iterates over output formula for *every* formula
    for lg_file in glob(osp.join(results_dir, "*.lg")):
        # Invoke lg2dot
        os.system("python $LgEvalDir/src/lg2dot.py " + lg_file + ">" +
                  osp.join(dot_out_dir,
                           osp.splitext(osp.split(lg_file)[1])[0] + ".dot"))

        # Invoke dot to generate PDF files
        os.system("dot -Tpdf " + osp.join(dot_out_dir,
                                          osp.splitext(osp.split(lg_file)[1])[0] + ".dot") + " -o "
                  + osp.join(dot_out_dir,
                             osp.splitext(osp.split(lg_file)[1])[0] + ".pdf"))

    logging.debug("  dot/PDF generation time: {:.4f}".format(time.time() - start)
                  + " seconds")


def create_htmls(filenames, input_dir, output_dir, model_name, last_epoch, MSP):
    start = time.time()
    logging.debug("\n[ Creating HTML Pages ]")

    # Creating paths/directories
    # RZ: Modification for new output formats.
    html_dir = MSP.HTML_OUT_DIR % output_dir
    pdf_html_dir = MSP.PARSE_DOT_OUT_DIR % output_dir

    tsv_out_dir = osp.join(MSP.PARSE_TSV_OUT_DIR % output_dir )
    file_num_pages = []

    ssd_img_dir = osp.basename(MSP.SCANSSD_VISUAL_DIR % output_dir)
    tsv_sym_img_dir = osp.basename(MSP.TSV_IN_VIS_DIR % output_dir)

    # Need to NOT use basename here...
    dot_pdf_dir = MSP.PARSE_DOT_OUT_DIR % output_dir

    logging.debug("  Output dir: {}".format(os.path.abspath(html_dir)))

    if isinstance(filenames, str):
        filenames = [filenames]

    for filename in filenames:
        pro_file = os.path.join(tsv_out_dir, filename + "-out.tsv")

        # AKS: Empty formula regions
        if not os.path.exists(pro_file):
            file_num_pages.append(0)
            continue

        image_path = osp.join(input_dir, "images", filename)
        num_pages = int(os.popen("ls " + image_path + " 2>/dev/null | wc -l") \
                        .read().strip('\n'))
        file_num_pages.append(num_pages)

        pdf_img_dir = osp.join(MSP.TSV_IN_VIS_DIR % output_dir, filename)
        pdf_html_out_dir = os.path.join(pdf_html_dir, filename)

        ( pro_list, docs_page_sizes ) =  read_pro_tsv(os.path.join(pro_file))
        pro_table = pro_list[0][1]

        create_pdfHTML(pro_table, input_dir, output_dir, pdf_html_out_dir, filename,
                       num_pages, pdf_img_dir, tsv_sym_img_dir, ssd_img_dir,
                       dot_pdf_dir, tsv_out_dir, MSP)

    create_mainHTML(html_dir, input_dir, filenames, file_num_pages)

    logging.debug("  HTML generation time: {:.4f} seconds".format(time.time() - start))
